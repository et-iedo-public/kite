#!/usr/bin/env jython

# Append the Flask and NetDB locations to the system path
import sys
#sys.path.append("/usr/lib/python2.7/dist-packages/")  # for debian 8
#sys.path.append("/usr/share/pyshared/")
sys.path.append("../../../kite_env/lib/python2.7/site-packages/")  # for debian 8
sys.path.append("../../../extra/netdb_rmi.jar")

from flask import Flask
from flask import request
#import IPython; IPython.embed()
from stanford.netdb import *

# Development NetDB RMI
RMI_SERVICE = "netdb-dev.stanford.edu"

# Create a NetDB datastore object and make it the default datastore
ds = NetDB_Datastore(RMI_SERVICE)
NetDB.default_datastore(ds)

# Do the Flask thing
app = Flask(__name__)
 
# Node info
@app.route("/nodes/<string:name>", methods=['GET'])
def getNode(name):
    node = Node.load(name)
    return node.toString()
 
# Search for nodes by admin team or group; return node names and departments
@app.route("/nodes", methods=['GET'])
def nodes():

    if request.args.has_key('admin-team'):
        ateam = request.args.get('admin-team', '')
        if ateam.find(' ') >= 0:
            ateam = '"' + ateam + '"'
        sp = Node_FS_Parameters()
        sp.admin_team(ateam)
        sp.include_node_name(True)
        sp.include_department(True)
        NetDB.autocomplete(False)

        result = "{ \"nodes\":\n  [\n"
        for node in Node.full_search(sp):
            result += "    { \"name\": \"%s\",  \"department\": \"%s\" },\n" \
                % (node.names()[0].full_name(), node.department().name())
        result =  result[:-2] + "\n  ]\n}\n"

    elif request.args.has_key('group'):
        group = request.args.get('group', '')
        if group.find(' ') >= 0:
            group = '"' + group + '"'
        sp = Node_FS_Parameters()
        sp.owner(group)
        sp.include_node_name(True)
        sp.include_department(True)
        NetDB.autocomplete(False)

        result = "{ \"nodes\":\n  [\n"
        for node in Node.full_search(sp):
            result += "    { \"name\": \"%s\",  \"department\": \"%s\" },\n" \
                % (node.names()[0].full_name(), node.department().name())
        result =  result[:-2] + "\n  ]\n}\n"
    else:
        result = 'bupkis'

    return result
 
if __name__ == "__main__":
    app.run()
