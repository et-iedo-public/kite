#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'TCG, Stanford University <tcg_comm@lists.stanford.edu>'  
__license__ = 'Apache License 2.0'  
"""
    QNetDB
    ~~~~~~~~
    Library to retrieve info from Stanford NetDB
    See https://stanfordits.atlassian.net/browse/TCG-424 for details.
"""
import sys
import logging
logging.basicConfig(stream=sys.stdout, level=logging.INFO)
logger = logging.getLogger(__name__)

app = Flask(__name__)

#class SetEncoder(json.JSONEncoder):
#    def default(self, obj):
#        if isinstance(obj, set):
#            return list(obj)
#        return json.JSONEncoder.default(self, obj)
#    
class QNetDB(object):
    server=None
    def __init__(self, server='whois.stanford.edu'):
        self.server=server
    def get_netdb_info(self, hostname):
        call(["ls", "-l"])


if __name__ == '__main__':
    return