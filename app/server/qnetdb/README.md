Python RPC library for Stanford's NetDB via NodeJS->NetDB RMI
# Requirements
- Install Java 8 JDK from [here](http://www.oracle.com/technetwork/java/javase/downloads/index.html)
- Install nodejs/npm from [here](https://nodejs.org/en/download/) or use Homebrew `brew install nodejs`

```
$ npm install
$ python setup.py install
```


# Example
```
from pyNetDB import Node

node = Node.load('cardinal.stanford.edu')
```
