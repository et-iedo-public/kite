#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'TCG, Stanford University <tcg_comm@lists.stanford.edu>'  
__license__ = 'Apache License 2.0'  
"""
    QDatadog
    ~~~~~~~~
    Library to retrieve info from Datadog
    See https://stanfordits.atlassian.net/browse/TCG-424 for details.
"""
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
import os.path
import logging
import StringIO
import requests
import json
import argparse
logging.basicConfig(stream=sys.stdout, level=logging.INFO)
logger = logging.getLogger(__name__)
import ConfigParser
from collections import defaultdict


import datadog
# Not sure why, but I need to import the whole pkg (above) AND then the two lines down... ?
from datadog import initialize, api

class QDatadog(object):
    account_hosts={}
    hosts=None
    tags={'tags':{}}
    lookup={}
    api_keys={}
    accounts={}
    subdomains={}
    times={}
    datadog_api={}
    def parse_config(self, config='datadog.ini'):
        conf = ConfigParser.RawConfigParser()   
        if os.path.isfile(config):
            configFilePath = config
            conf.read(configFilePath)
        else:
            buffer = StringIO.StringIO(config)
            conf.readfp(buffer)
        for account in conf.sections():
            self.api_keys[account]={'api_key': conf.get(account, 'api_key'), 'app_key': conf.get(account, 'app_key')}
            if conf.has_option(account, 'subdomain'):
                self.subdomains[account]= conf.get(account, 'subdomain')
        return self.accounts
    def __init__(self, config='datadog.ini'):
        self.parse_config(config)
        for account in self.api_keys.keys():
            logger.info('Initializing: %s',account)
            datadog.initialize(**self.api_keys[account])
            # Set main datadog API object per account, useful for interactive
            self.datadog_api[account]=datadog.api  
            # Now retrive tag->hosts per account
            response=datadog.api.Tag.get_all()
            if 'tags' not in response:
                logger.warning('%s not returning any tags, skipping account. Maybe removed?', account)
                continue
            tags=response['tags']
            self.account_hosts[account]=[]
            for tag_key, tag_hosts in tags.iteritems():
                sanitized_tagged_hosts=[x.replace('.stanford.edu','') for x in tag_hosts]
                if tag_key in self.tags:
                    self.tags[tag_key]+=sanitized_tagged_hosts
                else:
                    self.tags[tag_key]=sanitized_tagged_hosts
                self.account_hosts[account]+=sanitized_tagged_hosts
            self.account_hosts[account]=list(set(self.account_hosts[account])) # This forces uniqueness
            self.accounts[account]=len(tags)
            logger.info('Done initializing tags: %s',account)
        # Datadog apparently doesn't return easily the other way around, gotta build a dict with key the hostname
        hosts=[y for x in self.tags.values() for y in x]
        hosts=set(hosts) # This forces uniqueness
        hosts=list(hosts)
        logger.info('Done creating hosts list')
        for host_key in hosts:
#            logger.debug('Reverse lookup: processing host %s...',host_key)
            for tags_key, tags_value in self.tags.iteritems():
#                logger.debug('Reverse lookup: processing tags_key=%s which has %s elements',host_key, len(tags_value)) #,str(tags_key),str(tags_value))
                if host_key in tags_value:
                    if host_key in self.lookup and tags_key not in self.lookup[host_key]:
                        self.lookup[host_key]+=[tags_key]
                    else:
                        self.lookup[host_key]=[tags_key]
        self.hosts=self.lookup.keys()
        logger.info('Done initializing reverse lookup list for hostname=>tags')
    def get_correct_case_hostname(self, hostname):
        for x in self.hosts:
            if x.lower() == hostname.lower():
                logger.debug('Found right casing: %s', x)
                return x
    def get_account_by_hostname(self, hostname):
        for account in self.account_hosts.keys():
            if hostname.lower() in map(unicode.lower, map(lambda x: x.decode("utf-8"), self.account_hosts[account])):
                logger.debug('%s found in account %s, will use %s!', hostname, account, self.api_keys[account])
                return account
        return None
    def make_request(self, hostname, endpoint, params=None, method='GET'):
        account = self.get_account_by_hostname(hostname)
        if account not in self.api_keys:
            return None
        hostname=self.get_correct_case_hostname(hostname)
        s = requests.session()
        s.params = {
            'api_key': self.api_keys[account]['api_key'],
            'application_key': self.api_keys[account]['app_key'],
            'hostnames[]': [hostname.encode('ascii', 'ignore')]}
        result = s.request(method=method, url=endpoint, params=s.params)
#        import IPython; IPython.embed()
        return json.loads(result._content)
    def get_host_info(self, hostname):
        return self.make_request(hostname,
                                 endpoint='https://app.datadoghq.com/reports/v2/overview')
    def get_subdomain_by_hostname(self, hostname):
        account = self.get_account_by_hostname(hostname)
        if account in self.subdomains.keys():
            return self.subdomains[account]
        else:
            return 'app'
    def get_host_tags(self, hostname):
        result=[]
        info = self.get_host_info(hostname)
        if not info:
            return None
        logger.debug('QDatadog: get_host_info: %s', info)
        if 'rows' in info:
            if len(info['rows'])>0:
                if 'tags_by_source' in info['rows'][0]:
                    if 'Datadog' in info['rows'][0]['tags_by_source']:
                        result+=info['rows'][0]['tags_by_source']['Datadog']
                    if 'Users' in info['rows'][0]['tags_by_source']:
                        result+=info['rows'][0]['tags_by_source']['Users']
        return result
    def get_support_tag(self, hostname):
        tags = self.get_host_tags(hostname)
        if tags:
            result = filter(lambda x: x.startswith('support:'), tags)
            if len(result) > 0:
                return result[0]
        return 'None'

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--interactive', action='store_true',
        help="don't return, go into Ipython")
    parser.add_argument('-c', '--configfile', action='store', dest='config_file',
        help='Datadog INI file path containing all APP and API keys', required=True)
    parser.add_argument('-H', '--host', action='store', dest='host',
        help='host to query for Datadog info', required=True)
    args = parser.parse_args()

    qdd=QDatadog(config=args.config_file)

#    print 'Account hosts list'
#    print qdd.account_hosts
#    print 'Api keys'
#    print qdd.api_keys
    print 'HOST INFO'
    print qdd.get_host_info(args.host)
    print 'HOST TAGS'
    print qdd.get_host_tags(args.host)
    print 'SUPPORT TAG'
    print qdd.get_support_tag(args.host)

    if args.interactive:
        import IPython
        IPython.embed()
