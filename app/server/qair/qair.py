#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'TCG, Stanford University <tcg_comm@lists.stanford.edu>'
__license__ = 'Apache License 2.0'
"""
    qat
    ~~~~~~~~
    Retrieve info from Airtable
"""
import sys
import os
from flask import Flask, request, session, g, redirect, url_for, abort, render_template, flash, jsonify, make_response
from flask_httpauth import HTTPBasicAuth
from airtable import airtable
import numpy
import pandas
import re
from subprocess import Popen, PIPE
import argparse
import logging
logging.getLogger("urllib3").setLevel(logging.WARNING)
logger = logging.getLogger(__name__)


class AirTableBilling(object):
    __api_key = None
    atb = None
    def __init__(self, config_file='airtable.key', api_key=None):
        if api_key:
            self.__api_key = api_key.rstrip()
        else:
            self.__api_key = open(config_file,'r').read().rstrip()
        atb = airtable.Airtable('appslmNqC7c5UKIzB', __api_key)

    def search(self, field_name, field_value, record=None, **options):
        """
        Returns all matching records found in :any:`get_all`

        >>> airtable.search('Gender', 'Male')
        [{'fields': {'Name': 'John', 'Gender': 'Male'}, ... ]

        Args:
            field_name (``str``): Name of field to match (column name).
            field_value (``str``): Value of field to match.

        Keyword Args:
            maxRecords (``int``, optional): The maximum total number of records
                that will be returned. See :any:`MaxRecordsParam`
            view (``str``, optional): The name or ID of a view.
                See :any:`ViewParam`.
            fields (``str``, ``list``, optional): Name of field or fields to
                be retrieved. Default is all fields. See :any:`FieldsParam`.
            sort (``list``, optional): List of fields to sort by.
                Default order is ascending. See :any:`SortParam`.

        Returns:
            records (``list``): All records that matched ``field_value``

        """
        records = []
        formula = self.formula_from_name_and_value(field_name, field_value)
        options['formula'] = formula
        records = self.get_all(**options)
        return records

    def get_assets_byhost(self):

        something = atb.get('Assets')
        return something
