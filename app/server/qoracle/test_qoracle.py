#!/usr/bin/env python
# -*- coding: utf-8 -*- 
"""
    test_qoracle
    ~~~~~~~~
    
    __author__ = 'TCG, Stanford University <tcg_comm@lists.stanford.edu>'  
    __license__ = 'Apache License 2.0' 
"""
import pytest
import qoracle

class TestQOracle(object):
    o=None
    def test_empty_constructor(self):
        with pytest.raises(IOError):
            self.__class__.o = qoracle.QOracle()
    def test_constructor(self):
        self.__class__.o = qoracle.QOracle('test/test_oracle.csv')
        assert len(self.__class__.o.hosts)>10
    def test_query_for_unbilled_system(self):
        r = self.__class__.o.get_billing_info('farallone')
        assert r == ''
    def test_query_for_cmpv_247(self):
        r = self.__class__.o.get_billing_info('host9')
        assert r == 'CRCSG CMPV 247'
    def test_query_for_cplx_247(self):
        r = self.__class__.o.get_billing_info('host7')
        assert r == 'CRCSG CPLX 247'
    def test_query_for_cmpv_biz(self):
        r = self.__class__.o.get_billing_info('host11')
        assert r == 'CRCSG CMPV BIZ'
    def test_query_for_cplx_biz(self):
        r = self.__class__.o.get_billing_info('host14')
        assert r == 'CRCSG CPLX BIZ'
    def test_query_for_flex(self):
        r = self.__class__.o.get_billing_info('host13')
        assert r == 'CRCSG FLEX'
