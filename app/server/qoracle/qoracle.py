#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'TCG, Stanford University <tcg_comm@lists.stanford.edu>'  
__license__ = 'Apache License 2.0'  
"""
    qoracle
    ~~~~~~~~
    Retrieve info from Airtable
"""
import sys
import os
import pandas
import argparse
import logging
logging.basicConfig(stream=sys.stdout, level=logging.INFO, format='%(asctime)s %(message)s')
logger = logging.getLogger(__name__) # pylint: disable=locally-disabled, invalid-name

class QOracle(object):
    df=None
    hosts=None
    def __init__(self, csvfile='oracle.csv'):
        logger.info('Loading billing CSV file {0}...'.format(csvfile))
        if not os.path.exists(csvfile):
            raise IOError('Make sure csvfile path exists! just tried to use {0}'.format(csvfile))
        self.df = pandas.read_csv(csvfile)
        self.df = self.df[['Service Identifier 1','Item Code','Total Charge']]
        self.df.columns = ['Hostname','BillType','Charge']
        self.df = self.df[self.df['Hostname'].notnull()]
        self.df = self.df[self.df['BillType']!='CRC CRASHPLAN']
        self.df = self.df[self.df['BillType']!='CRC FST 500']
        self.df = self.df[self.df['BillType']!='CRC CONSULT']
        self.df = self.df[self.df['BillType']!='CRC FST LAB']
        self.df = self.df[self.df['BillType']!='LABOR CRC SYS ADMIN']
        self.df = self.df[self.df['BillType']!='CRC FST 200']
        self.df = self.df[self.df['BillType']!='LABOR CRC ND O']
        self.df = self.df[self.df['BillType']!='CRC FST 501']
        self.df = self.df[self.df['BillType']!='CREDIT CRC O']
        self.df = self.df[self.df['BillType']!='LABOR CRC ND']
        self.df = self.df[self.df['BillType']!='CRC FST PREM']
        self.df = self.df[self.df['BillType']!='CRC FST LIM']
        # Some fields are with the FQDN, but we really wanna care about the PQDN only
        self.hosts=self.df['Hostname'].replace(regex=True,inplace=True,to_replace=r'.stanford.edu$',value=r'')
        self.hosts=self.df['Hostname'].values.tolist()
#        import IPython; IPython.embed()
    def get_billing_info(self, hostname):
        return self.df[pandas.Series(self.df.Hostname).str.match('^'+hostname+'$', case=False)].BillType.to_json(orient='values').strip('[]""')
#        return self.df[self.df.Hostname==hostname].BillType.to_json(orient='values').strip('[]""')

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--interactive', action='store_true',
        help="don't return, go into Ipython")
    parser.add_argument('-c', '--csv', action='store', dest='csv_file',
        help='Oracle CSV full path', required=True)
    parser.add_argument('-H', '--host', action='store', dest='host',
        help='host to query for billing info', required=True)
    args = parser.parse_args()

    qoracle=QOracle(csvfile=args.csv_file)
    print qoracle.get_billing_info(args.host)

    if args.interactive:
        import IPython
        IPython.embed()