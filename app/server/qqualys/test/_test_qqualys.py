#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'TCG, Stanford University <tcg_comm@lists.stanford.edu>'  
__license__ = 'Apache License 2.0'  

# Accomodating multiple versions of Python
from sys import version_info
if version_info < (2, 7):  # pragma: no cover
    import unittest2 as unittest
else:
    import unittest
try:
    from unittest.mock import patch, call
except ImportError:
    from mock import patch, call

import mock
import sys
import os.path
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
from qqualys import Qapi, resolve_to_ip, is_ip_address

class  QqualysTestCase(unittest.TestCase):
    def setUp(self):
        self.q = Qapi()
    def test_is_ip_address_function(self):
        self.assertTrue(is_ip_address('1.2.3.4'))
        self.assertFalse(is_ip_address('a.b'))

    def test_resolve_to_ip_function(self):
        with mock.patch('qqualys.is_ip_address') as MockMethod:
            MockMethod.return_value = '8.8.8.8'
            self.assertEqual(resolve_to_ip('8.8.8.8'),'8.8.8.8')
            MockMethod.return_value = False
            self.assertEqual(resolve_to_ip('google-public-dns-a.google.com'),'8.8.8.8')
            self.assertEqual(resolve_to_ip('inexistent.host.name.test'),None)    
        
    def test_qapi_class(self):
        with mock.patch.object(Qapi, 'request') as MockClassMethod:
            MockClassMethod.return_value = open('oxml.out','r').read()        
            self.assertNotEqual('Qapi.oxml',None)       
#        with mock.patch('Qapi.get_host_vulnerabilities') as MockClassMethod:
#            MockClassMethod.return_value = [(1,2,3)]
#        self.assertEqual(resolve_to_ip('8.8.8.8'),'8.8.8.8')
#        with mock.patch('Qapi.is_ip_address') as MockClass:
#            MockClass.return_value = False
#        self.assertEqual(resolve_to_ip('google-public-dns-a.google.com'),['8.8.8.8'])
        
#    def test_test_qqualys(self):
#        #assert x != y;
#        #self.assertEqual(x, y, "Msg");
#        self.fail("TODO: Write test")

if __name__ == "__main__":
    unittest.main(verbosity=2)