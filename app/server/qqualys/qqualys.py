#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'TCG, Stanford University <tcg_comm@lists.stanford.edu>'
__license__ = 'Apache License 2.0'

# A MUCH better rewrite of this tool to be found here: https://code.stanford.edu/et-iedo-public/qqualys
# Never had the time to do the legwork to put this up to speed, especially considering Kite has been
# abandoned by TCG and Marcello has no reason to keep developing this tool for his new teams.

from lxml import objectify
import datetime
import qualysapi
import argparse
import socket # to check IP address validity
import os
import re
import operator
import requests
import socket
import ConfigParser
import StringIO
from pprint import pprint
from qualysapi.connector import QGConnector
import qualysapi.config as qcconf

import logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

def is_ip_address(addr):
    try:
        socket.inet_aton(addr)
    except socket.error:
        return False
    return addr
def resolve_to_ip(host):
    if is_ip_address(host):
        logger.debug('IP passed: %s', host)
        return host
    else:
        # the gethostbyname syscall returns a 3-ple, index 2 is array of IP(s)
        logger.debug('Name passed: %s', host)
        if '.' not in host:
            host += '.stanford.edu'
        try:
            return socket.getaddrinfo(host, None)[0][4][0]
        except socket.gaierror:
            return None
def get_pqdn(host):
    return host.lower().replace('.stanford.edu', '')
def get_fqdn(host):
    if '.' in host:
        return host.lower()
    else:
        return host.lower()+'.stanford.edu'

# Monkey-patching qualysapi lib, as pull requests would take forever to process.
# Apparently the dev of this lib ONLY love dealing with the ini file...
def connect(config, remember_me=False, remember_me_always=False, username=None, password=None, hostname=None):
    """ Return a QGAPIConnect object for v1 API pulling settings from config file. """
    connect = None
    conf = ConfigParser.RawConfigParser()
    if os.path.isfile(config):
        with open(config, 'r') as f:
            config = f.read()
    if config:
        try:
            buffer = StringIO.StringIO(config)
            conf.readfp(buffer)
            connect = QGConnector((conf.get('info', 'username'),conf.get('info', 'password')),
                                  conf.get('info', 'hostname'),
                                  None, 3)
        finally:
            del conf
            del buffer
            del config
    else:
        connect = QGConnector((username,password),
                              hostname,
                              None,
                              3)
    logger.info("Finished building connector.")
    return connect
qualysapi.connect=connect

class Qapi:
    hosts = []
    url = None
    scanners = None
    cache = None
    headers = {'X-Requested-With': 'Kite via qualysapi', 'Accept': 'application/xml', 'Content-type': 'text/xml'}
    """Wraps all the boring redundancy for Qualys api calls, e.g. the URI"""
    # user_login='nbfa@stanford.edu' # By default, all scans seem to be owned by Noah
    def __init__(self, config='qualys_config.ini', remember_me=False, remember_me_always=False, username=None, password=None, hostname=None, cache=None):
        self.qgc=qualysapi.connect(config=config, remember_me=False, remember_me_always=False, username=None, password=None, hostname=None)
        conf = ConfigParser.RawConfigParser()
        buffer = StringIO.StringIO(config)
        conf.readfp(buffer)
        self.url = 'https://' + self.qgc.server
        self.scanners = ','.join(self.get_scanner_appliances_list())
        self.cache = cache
    def _make_get_params(self, params):
        if params:
            return '?'+'&'.join('{0}={1}'.format(key, value) for key, value in params.items())
        else:
            return ''
    def _post_xml(self, uri, params, headers=None, raw_output=False):
        url = self.url + uri
        headers = self.headers if not headers else headers
        response = requests.post(url, data=params, headers=headers, auth=self.qgc.auth)
        logging.info('POST: {}, params: {}, response: {}'.format(url, params, response))
        oxml = objectify.fromstring(response.text.encode('utf-8'))
        if raw_output:
            return response.text
        else:
            return oxml
    def _get_xml(self, uri, params, headers=None, raw_output=False):
        url = self.url + uri + '/' + self._make_get_params(params)
        headers = self.headers if not headers else headers
        response = requests.get(url, headers=self.headers, auth=self.qgc.auth)
        logging.info('GET: {}'.format(url))
        if raw_output:
            return response.text
        else:
            oxml = objectify.fromstring(response.text.encode('utf-8'))
            return oxml
    def request(self, parameters, base_uri):
        logging.info('API call parameters: {0}'.format(parameters))
        raw_output = self.qgc.request(base_uri, parameters)
        try:
            return objectify.fromstring(raw_output)
        except xmlParseEntityRef:
            logger.info('Raw XML response: %s', raw_output)
    # I couldn't get the qualysapi python lib to work with remediation tickets, going manual
    def get_scanner_appliances_list(self):
        oxml = self._get_xml('/api/2.0/fo/appliance', {'action': 'list'})
        scanners = [str(x) for x in oxml.xpath('//RESPONSE/APPLIANCE_LIST/*[STATUS="Online"]/NAME') ]
        return scanners
    def get_scanner_appliances_given_fwzone(self, fwzone):
        scanners = [str(x) for x in self.scanners.split(',') if re.match(".*{}.*$".format(fwzone.upper()),str(x))]
        return ','.join(scanners)
    def get_rest_api_version(self):
        return self._get_xml('/qps/rest/portal/version', None, headers={'Accept': 'application/xml'}, raw_output=True)
    def get_agents_by_tag_xml(self, tag='mg-TCG'):
        params = """<ServiceRequest>
             <filters>
                <Criteria field="tagName" operator="EQUALS">{tag}</Criteria>
             </filters>
            </ServiceRequest>""".format(tag=tag)
        return self._post_xml('/qps/rest/2.0/search/am/hostasset', params)
    def get_agent_by_dns(self, hostname, operator='EQUALS'):
        params = """<ServiceRequest>
             <filters>
                <Criteria field="dnsHostName" operator="{operator}">{hostname}</Criteria>
                <Criteria field="agentConfigurationName" operator="CONTAINS">TCG</Criteria>
             </filters>
            </ServiceRequest>""".format(hostname=hostname, operator=operator)
        return self._post_xml('/qps/rest/2.0/search/am/hostasset', params)
    def get_asset_details_xml(self, asset_id):
        headers = {"Content-type": "text/xml"}
        return self._get_xml('/qps/rest/2.0/get/am/hostasset/'+str(asset_id), None, headers={"Content-type": "text/xml"})
    def get_asset_id(self, hostname):
        if hostname.isdigit():
            return int(hostname)
        else:
            if self.cache:
                asset_id = self.cache.get(hostname, 'asset_id')
                if asset_id:
                    logger.info('%s was found in DynamoDB as asset_id: %s', hostname, asset_id)
                    return int(asset_id)
            # else:
            oxml = self.get_agent_by_dns(get_fqdn(hostname))
            if not 'data' in oxml.__dict__:
                oxml = self.get_agent_by_dns(get_pqdn(hostname), operator='CONTAINS')
            # import IPython; IPython.embed()
            if oxml.count == 1:
                asset_id = oxml.xpath('//data/HostAsset/id')[0]
                logger.info('%s has HostAsset/id => %s', hostname, asset_id)
                if self.cache:
                    logger.info('Caching %s in DynamoDB as asset_id: %s', hostname, asset_id)
                    self.cache.set(hostname, {'asset_id': asset_id})
                return int(asset_id)
        return None
    def get_sw_inventory(self, host):
        asset_id = self.get_asset_id(host)
        oxml = self.get_asset_details_xml(asset_id)
        software_list = {}
        for sw in oxml.xpath('//HostAssetSoftware'):
            software_list[sw.name.text.encode('utf-8')] = sw.version.text.encode('utf-8')
        return software_list
    def populate_agent_hosts(self):
        oxml = self.get_agents_by_tag_xml()
        self.hosts = oxml.xpath('//data/HostAsset/dnsHostName')
        self.hosts = [x.replace('.stanford.edu','') for x in self.hosts]
    def start_new_scan(self, hostname):
        # import IPython; IPython.embed()
        asset_id = self.get_asset_id(hostname)
        oxml = self.get_asset_details_xml(asset_id)
        scanner_tags = oxml.xpath('//data/HostAsset/tags/list/*/name[contains(text(), "x-")]')
        params = {'action': 'launch',
                'scan_title': 'TCG_scanning_for_'+ hostname,
                'ip': resolve_to_ip(hostname),
                'option_title': 'ISO Official 3/4/5 (Site-wide)'}
        # The following block is to get the fw zone tag to then use
        associated_scanners = []
        for scanner_tag in scanner_tags:
            match = re.match('^x-([\w\d]+).*', str(scanner_tag))
            if match:
                associated_scanners.append(match.group(1))
                break # Will support multiple scanners later if needed
        if associated_scanners:
            params['iscanner_name'] = self.get_scanner_appliances_given_fwzone(associated_scanners[0]) # set enforces uniqueness in a list
        elif self.scanners:
            params['iscanner_name'] = self.scanners
        else:
            params['default_scanner'] = '1'
        # import IPython; IPython.embed()
        # DO NOT ask me why, but somehow one day I realized start new scan is a post call that needs get style params???
        return self._post_xml('/api/2.0/fo/scan/'+self._make_get_params(params), params={}, raw_output=True)
    def get_scans_list(self, username=None):
        if not username:
            username = self.qgc.auth[0]
        oxml = self._get_xml('/api/2.0/fo/scan/', {'action': 'list', 'state': 'Finished,Running,Queued,Loading'})
        listing = []
        for s in oxml.xpath('//RESPONSE/SCAN_LIST/SCAN[USER_LOGIN="{}"]'.format(username)):
            listing.append((s.TITLE, s.TARGET, s.LAUNCH_DATETIME, s.DURATION, s.STATUS.STATE))
        logger.info("Found %d vulnerabilities", len(listing))
        return listing
    def get_scans_as_ul_li_list(self):
        listing = self.get_scans_list()
        array=[]
        for item in listing:
            array+=['{0} ({1}): {2} [{4}]'.format(
                item[0], item[1], item[2], item[3], item[4])]
        return array
    def get_remediation_tickets(self, hostname):
        params = { 'show_vuln_details': 1, 'states': 'OPEN'}
        if is_ip_address(hostname):
            params.update({'ips': hostname})
        else:
            params.update({'dns_contains': hostname})
        oxml = self._get_xml('/msp/ticket_list.php', params)
        tnow=datetime.datetime.now()
        # since "dns-contains" is not a exact match search):
        tickets = oxml.xpath('//TICKET_LIST/TICKET[VULNINFO[SEVERITY>3]][DETECTION[DNSNAME[text() ="{0}.stanford.edu" or text()="{0}"]]]'.format(get_pqdn(hostname)))
        # import IPython; IPython.embed()
        results = []
        for t in tickets:
            tfound=datetime.datetime.strptime(str(t.CREATION_DATETIME),'%Y-%m-%dT%H:%M:%SZ')
            age=tnow-tfound
            # Had to put a limit since some badly managed servers had so many chunky vulnerabilities to load, that they would hang Flask in composing the pages.
            diagnosis = t.DETAILS.DIAGNOSIS.text.encode('utf-8') if len(tickets)<15 else ''
            solution = t.DETAILS.SOLUTION.text.encode('utf-8') if len(tickets)<15 else ''
            results.append((int(t.VULNINFO.QID),
                            int(t.VULNINFO.SEVERITY),
                            t.VULNINFO.TITLE.text.encode('utf-8'),
                            t.HISTORY_LIST.HISTORY.SCAN.REF.text.encode('utf-8')[:5],
                            int(age.days),
                            t.DETAILS.CONSEQUENCE.text.encode('utf-8'),
                            diagnosis,
                            solution))
        logger.info("Found %d remediation tickets", len(results))
        return results
    def get_remediation_tickets_list(self, hostname):
        listing = self.get_remediation_tickets(get_pqdn(hostname))
        # sort by source first (a in agent comes first, but it's less important than VM ('v') so reversing
        listing = sorted(listing, key=operator.itemgetter(3,1,4), reverse=True)
        return listing
    # List all hosts known by Qualys in a given ip range
    def get_hosts_by_ip_range(self, ipstart, ipend):
        params={ 'action': 'list', 'ips': '%s-%s' % (ipstart, ipend) }
        self.request(params, '/api/2.0/fo/asset/host/')
    # List all vulnerabilities found so far in previous scans
    def get_vulnerabilities_scan_list(self):
        params={ 'action': 'list', 'state':'Finished' }
        self.request(params,'/api/2.0/fo/scan/')
    # Get host info about a specific IP address or hostname
    def get_host_vulnerabilities(self, host):
        params={ 'action': 'list', 'ips': resolve_to_ip(host), 'show_igs':1}
        oxml=self.request(params,'/api/2.0/fo/asset/host/vm/detection')
        detections = oxml.xpath('//RESPONSE/HOST_LIST/HOST[1]/DETECTION_LIST/DETECTION[SEVERITY>2]')
        logging.debug('TOTAL DETECTIONS: {0}'.format(len(detections)))
        tnow=datetime.datetime.now()
        list=[]
        for d in detections:
            try:
                    tfound=datetime.datetime.strptime(str(d.FIRST_FOUND_DATETIME),'%Y-%m-%dT%H:%M:%SZ')
                    age=tnow-tfound
                    list.append((int(d.QID), int(d.SEVERITY),int(age.days)))
            except AttributeError:
                continue
        sorted_list=sorted(list, key=lambda row: row[2], reverse=True)
        return sorted_list
    def get_qid_info(self, qid):
        params={ 'action': 'list', 'ids': qid, 'details': 'Basic', 'echo_request': 1}
        oxml=self.request(params,'/api/2.0/fo/knowledge_base/vuln')
        try:
            qid_out=oxml.RESPONSE.VULN_LIST.getchildren()
        except AttributeError:
            return None
        return qid_out
    def get_qid_summary_list(self,host):
        vulns=self.get_host_vulnerabilities(host)
        qid_list=','.join([str(i[0]) for i in vulns])
        results=self.get_qid_info(qid_list)
        array=[]
        if results:
            for row in results:
                data= row.getchildren()
                array+=['{0} ({1}): {2}'.format(data[0], data[2], data[3])]
        return array

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    # Works and tested
    parser.add_argument('-c', '--config', action='store', dest='config_file', help='Qualys Config file full path', required=True)
    parser.add_argument('-i', '--interactive', action='store_true', help="don't return, go into Ipython")
    parser.add_argument('-V', '--version', action='store_true', help="Show rest API version supported by server")
    parser.add_argument('-l', '--list_scans', action='store_true', help="List all available vulnerabilities scans")
    parser.add_argument('-S', '--list_scanners', action='store_true', help="List the scanners available")
    parser.add_argument('-a', '--appliances-lookup-by-fwzone-tag', action='store', dest='appliance_lookup', help='Get appliances associated to given FW zone tag (e.g. FOA, FOA2, etc')
    parser.add_argument('-H', '--host', action='store', dest='host', help='host to query for known found vulnerabilities')
    parser.add_argument('--lookup', action='store', dest='lookup', help='Resolve to host Asset id')

    # parser.add_argument('-Q', '--query-host-asset', action='store', dest='query', help='Get host asset XML v2')

    # ToDo verify the following:
    parser.add_argument('-s', '--scan', action='store_true', help="Scan the server specified with -H")
    parser.add_argument('-u', '--username', action='store', dest='username', help='username used for actions in Qualys')
    parser.add_argument('--hostid', action='store', dest='hostid', help='Host Asset id')
    parser.add_argument('-A', '--list_agents', action='store_true', help="EXPERIMENTAL List all available agents and their info")
    parser.add_argument('-I', '--show_software_info', action='store_true', help="Show software info for QCA host asset")
    parser.add_argument('-t', '--tag', dest='tag', help="List all available agents and their info")
    parser.add_argument('--ipstart', action='store', dest='ipstart', type=is_ip_address, help='IP range Start')
    parser.add_argument('--ipend', action='store', dest='ipend', type=is_ip_address, help='IP range End')
    parser.add_argument('-r', '--report-on', action='store', dest='target', help='host to report on for known found vulnerabilities - high level')
    parser.add_argument('-q', '--qid', action='store', dest='qid', help='QID info to show (can be also range e.g. 90023-90040)')
    args = parser.parse_args()
    config = None
    with open(args.config_file, 'r') as f:
        config = f.read()
    q = Qapi(config=config)

    if args.version:
        print q.get_rest_api_version()
    elif args.lookup:
        print q.get_asset_id(args.lookup)
    elif args.appliance_lookup:
        print q.get_scanner_appliances_given_fwzone(args.appliance_lookup)
    # elif args.query:
    #     print q.get_hostasset_xml(args.query)
    elif args.list_scanners:
        print q.get_scanner_appliances_list()
    elif args.list_agents:
        print q.get_agents_by_tag_xml(args.tag)
    elif args.show_software_info:
        if args.hostid:
            print q.get_sw_inventory(args.hostid)
        else:
            print q.get_sw_inventory(args.host)
    elif args.ipstart is not None and args.ipend is not None:
        q.get_hosts_by_ip_range(args.ipstart, args.ipend)
    elif args.list_scans:
        print q.get_scans_list(args.username)
    elif args.scan and args.host:
        print 'Starting new scan'
        print q.start_new_scan(args.host)
    elif args.host:
        sorted_list = q.get_remediation_tickets_list(args.host)
        print sorted_list
#        if sorted_list:
#            for row in sorted_list:
#                print 'QID: {0} | SEV: {1} | AGE(days): {2}'.format(row[0], row[1], row[2])
    elif args.target:
        sorted_list=q.get_qid_summary_list(args.target)
    elif args.qid:
        sorted_list=q.get_qid_info(args.qid)
        if sorted_list:
            if len(sorted_list)>1:
                for row in sorted_list:
                    data= row.getchildren()
                    print 'QID: {0} | SEV: {1} | Title: {2}'.format(data[0], data[2], data[3])
            else:
                pprint(sorted_list[0].getchildren())
    else:
        pass

    if args.interactive:
        import IPython; IPython.embed()
