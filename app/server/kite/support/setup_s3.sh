#!/usr/bin/env bash
BUCKET=tcg-kite-secrets
POLICY=tcg-kite-secrets_policy.json
aws s3 mb s3://${BUCKET}
aws s3api put-bucket-policy --bucket ${BUCKET} --policy file://${POLICY}
# test upload of this very script there:
aws s3 cp $0 s3://${BUCKET} --sse
aws s3 ls s3://${BUCKET} 
