#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
    Kite
    ~~~~~~~~
    WebApp and API service that returns all the info related to a specific host.
    In future it'll handle multiple hosts requests at the same time, and will do
    cross-reference and reconciliation (e.g. DataDog tags consistent with what
    we have in Oracle, etc.)
    See https://stanfordits.atlassian.net/browse/TCG-424 for details.
    __author__ = 'TCG, Stanford University <tcg_comm@lists.stanford.edu>'
    __license__ = 'Apache License 2.0'
"""

import os
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
import logging
import time
import argparse
from multiprocessing import Process, Queue
import json
import yaml
import re
import ConfigParser
import StringIO
import uuid
from flask import (
    jsonify,
    session,
    Flask,
    request,
    Response,
    render_template,
    redirect,
    flash,
    url_for,
    Markup,
    send_from_directory
)
from healthcheck import HealthCheck, EnvironmentDump
from flask_wtf import FlaskForm as Form
from wtforms import StringField, BooleanField
from wtforms.validators import DataRequired
from werkzeug.utils import secure_filename
from flask_httpauth import HTTPBasicAuth
from json2html import *  # pylint: disable=locally-disabled, wildcard-import, unused-wildcard-import
sys.path.append('../qoracle')
import qoracle
sys.path.append('../qqualys')
import qqualys
sys.path.append('../qjira')
import qjira
sys.path.append('../qat')
import qat
sys.path.append('../qdatadog')
import qdatadog
sys.path.append('../qsnow')
import qsnow
sys.path.append('../qbigfix')
import qbigfix
sys.path.append('../common/common')
import kms_s3_encrypt
import mgdynamodb
#import IPython; IPython.embed()

# Parsing options right away to avoid loading the whole deal before complaining about arguments being wrong.
# Another section like this at the end.
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--interactive', action='store_true',
                        help="don't return, go into Ipython")
    parser.add_argument('-R', '--reconcile', action='store_true',
                        help='Bulk reconciliation mode. By default writes comments in a Kiosk column, or to file if -o option is specified')
    parser.add_argument('--fast', action='store_true',
                        help='Bulk reconciliation mode fast processing, just evaluates any server that has "reconciliation" field still blank')
    parser.add_argument('--all', action='store_true',
                        help='Bulk reconciliation mode full recomputation of all records except those with Service End Date populated')
    parser.add_argument('-o', '--outputfile', action='store', dest='output_file',
                        help='optional output file for bulk reconciliation mode')
    args = parser.parse_args()
    if ((args.fast and not args.all) and (not args.fast and args.all) and not args.reconcile) or (args.fast and args.all):
        parser.error(
            "--reconcile (-R) could require either --fast or --all but  at the same time")

logging.basicConfig(stream=sys.stdout, level=logging.INFO,
                    format='%(asctime)s %(levelname)s %(message)s')
logging.getLogger("urllib3").setLevel(logging.INFO)
logger = logging.getLogger(
    __name__)  # pylint: disable=locally-disabled, invalid-name

APP = Flask(__name__)


def regex_replace(s, find, replace):
    """A non-optimal implementation of a regex filter"""
    return re.sub(find, replace, str(s))


APP.jinja_env.filters['regex_replace'] = regex_replace

APP.secret_key = str(uuid.uuid4())
if os.environ.get('KITE_UPLOADS_DIR'):
    APP.config['UPLOAD_FOLDER'] = os.environ['KITE_UPLOADS_DIR']
else:
    APP.config['UPLOAD_FOLDER'] = '../../../uploads'
if os.environ.get('KITE_CONFIG_DIR'):
    APP.config['CONFIG_FOLDER'] = os.environ['KITE_CONFIG_DIR']
else:
    APP.config['CONFIG_FOLDER'] = '../../../config'
if os.environ.get('KITE_SECRETS_DIR'):
    APP.config['SECRETS_FOLDER'] = os.environ['KITE_SECRETS_DIR']
else:
    APP.config['SECRETS_FOLDER'] = '/opt/tcg/secrets'
APP.config['MAX_CONTENT_LENGTH'] = 25 * 1024 * 1024
KITE_AT_API_SLEEP_SECS = 1
AUTH = HTTPBasicAuth()

AKS = kms_s3_encrypt.AwsSecret(os.environ['KITE_KMS_IAM_USER'],
                               os.environ['KITE_MASTER_KEY'],
                               os.environ['KITE_S3_SECRETS_BUCKET'],
                               aws_access_key_id=os.environ.get(
                                   'aws_access_key_id'),
                               aws_secret_access_key=os.environ.get('aws_access_key_id'))
DDB = mgdynamodb.MGDynamoDB(
    'kite_hosts', key='hostname', default_attr='asset_id')
CONF = ConfigParser.RawConfigParser()
CONF.readfp(StringIO.StringIO(AKS.s3_secrets_read('setup.ini')))
USERS = dict(CONF.items('Users'))


@AUTH.get_password
def get_pw(username):
    """check for auth"""
    if username in USERS:
        return USERS.get(username)
    return None


BILLING_ANSWERS = AKS.s3_secrets_read_json_as_dict('billing_answers.json')

QJ = qjira.QJira(config=AKS.s3_secrets_read('jira.ini'))
logger.info('qjira module loaded')
QQ = qqualys.Qapi(config=AKS.s3_secrets_read('qualys_config.ini'), cache=DDB)
logger.info('qqualsy module loaded')
QDD = qdatadog.QDatadog(config=AKS.s3_secrets_read('datadog.ini'))
logger.info('qdatadog module loaded, %s hosts found', len(QDD.hosts))
QAT = qat.AirTableApi(config=AKS.s3_secrets_read(
    'airtable.ini'), schema_json=AKS.s3_secrets_read_json_as_dict('airtable_schema.json'))
logger.info('qat module loaded, %s hosts found', len(QAT.hosts))
QS = qsnow.QSnow(AKS.s3_secrets_read('servicenow.ini'))
logger.info('qsnow module loaded')
AKS.download_file('oracle.csv', APP.config['UPLOAD_FOLDER']+'/oracle.csv')
QO = qoracle.QOracle(csvfile=APP.config['UPLOAD_FOLDER']+'/oracle.csv')
logger.info('qoracle module loaded, %s hosts found', len(QO.hosts))
AKS.s3_secrets_dump_to_file(
    'bfcreports_stanford_edu.pem.gz', '/tmp/bfcreports_stanford_edu.pem.gz')
AKS.gunzip_file('/tmp/bfcreports_stanford_edu.pem.gz',
                APP.config['SECRETS_FOLDER']+'/'+'bfcreports_stanford_edu.pem', remove_original_gz_file=True)
QB = qbigfix.QBigFix(config=AKS.s3_secrets_read('bigfix.ini'))
if hasattr(QB, 'hosts'):
    logger.info('qbigfix module loaded, %s hosts found', len(QB.hosts))
MATRIX = yaml.safe_load(AKS.s3_secrets_read('consistency_matrix.yaml'))
#import IPython; IPython.embed()

PQUEUE = Queue()
logger.info('created Queue()')

health = HealthCheck(APP, "/healthcheck")
FLAG_RESTART = False
def check_app_ok():
    return True, "App OK" if FLAG_RESTART is False else False
health.add_check(check_app_ok)
def application_data():
	return {"maintainer": "Marcello Golfieri (golfieri@stanford.edu)",
            "support": "tcg@office365stanford.onmicrosoft.com",
	        "git_repo": "https://code.stanford.edu/tcg/kite"}
envdump = EnvironmentDump(APP, "/environment",
    include_python=False, include_os=False,
    include_process=False, include_config=False)
envdump.add_section("application", application_data)

def request_wants_json():
    """determine what to return, is it a browser or a api call?"""
    best = request.accept_mimetypes.best_match(
        ['application/json', 'text/html'])
    return best == 'application/json' and \
        request.accept_mimetypes[best] > \
        request.accept_mimetypes['text/html']


def render_json_as_table(json_output, heading='', color='', form=None):
    """helper to return json in a prettier way for poor humans"""
    return render_template('table_output.html', heading2=Markup(heading), color=color,
                           text=Markup(json2html.convert(json=json_output.response[0], escape=False)), form=form)


class SetEncoder(json.JSONEncoder):
    """needed to make the whole json thingy work (don't remember now why)"""

    def default(self, obj):  # pylint: disable=locally-disabled, method-hidden
        if isinstance(obj, set):
            return list(obj)
        return json.JSONEncoder.default(self, obj)


class Kite(object):  # pylint: disable=locally-disabled, too-few-public-methods
    """Main Kite class, that holds all the hosts across datasources"""
    qo = None
    qat = None
    qdd = None
    all_hosts = None  # Dinamically generated list from all our datasources
    billing_answers = BILLING_ANSWERS

    def __init__(self, qo, qat, qdd, qb=None):
        """constructor, pretty much it just builds up the whole hosts list
        from other datasources"""
        # Set is to enforce uniqueness
        self.qo = qo
        self.qat = qat
        self.qdd = qdd
        self.qb = []
        hosts = qo.hosts + qat.hosts + qdd.hosts
        if qb and hasattr(qb, 'hosts'):
            self.qb = qb
            hosts += qb.hosts
        self.all_hosts = list(set(hosts))
        self.all_hosts = [x.encode('utf-8') for x in self.all_hosts]
        self.all_hosts = sorted(list(set(map(lambda x:x.lower(), self.all_hosts))))
    @classmethod
    def check_support_consistency(self, response):
        logger.debug('*** EVALUATING {}'.format(response))
        if response.get('get_kiosk_info'):
            kiosk = response.get('get_kiosk_info').get('fields')
            service_level = kiosk.get('Service Level')
        else:  # can't get AirTable data. Given it's so central, won't continue further
            FLAG_RESTART = True
            return {'status': 'ERROR', 'color': 'red', 'reasons': ['AirTable information not found, or server disconnected. Please restart application']}
        oracle_billing = response.get('get_billing_info') if response.get(
            'get_billing_info') else 'None'
        # The following split is because for some reason now Airtable returns the "Active System Count integer" appended with a comma if present...
        kiosk_billing = kiosk.get('Bill Code').split(',')[0] if kiosk.get('Bill Code') else False

        if kiosk.get('Service End Date'):  # Host has been decommissioned.
            result = {'status': 'DECOMM', 'color': 'gray',
                      'reasons': ['Decommissioned']}
        # elif oracle_billing != kiosk_billing:
        #     result.update({'reasons' : ['Billing mismatch between Kiosk and Oracle']})
        elif not response.get('get_host_tags'):
            result = {'status': 'ERROR', 'color': 'red',
                      'reasons': ['No Datadog tags found']}
        else:
            datadog_support_tag = filter(lambda x: x.startswith(
                'support:'), response.get('get_host_tags')) or ['support:247']
            result = {'status': 'ERROR', 'color': 'red',
                      'reasons': ['Invalid configuration (Oracle: {}, Kiosk Bill code: {}, DD: {}'.format(
                          oracle_billing, kiosk_billing, datadog_support_tag, service_level)]}

            def matching(real, expected):
                if isinstance(real, list) and len(real) > 0:
                    real = real[0]
                expected = [] if (
                    expected is False or expected is None) else expected
                return True if (real in expected) else False
            # Sorted so I can evaluate in the order I need by prepending the right index number
            for category in sorted(MATRIX):
                combo = MATRIX[category]
                logger.debug('Kiosk Billing match? {} (between {} and {}'.format(matching(
                    kiosk_billing, combo['kiosk_billing']), kiosk_billing, combo['kiosk_billing']))
                logger.debug('Oracle Billing match? {} (between {} and {}'.format(matching(
                    oracle_billing, combo['oracle_billing']), oracle_billing, combo['oracle_billing']))
                logger.debug('DD match? {} (between {} and {}'.format(matching(
                    datadog_support_tag, combo['datadog']), datadog_support_tag, combo['datadog']))
                # import IPython; IPython.embed()
                if (matching(kiosk_billing, combo['kiosk_billing']) and
                    matching(oracle_billing, combo['oracle_billing']) and
                        matching(datadog_support_tag, combo['datadog'])):
                    result.update(
                        {'status': 'OK', 'color': 'green', 'reasons': []})
                    break
        return result

    def bulk_reconcile(self, targets=None, output=None):
        """method to mass-check for consistency between
        billing, support level and kiosk"""
        logger.debug(
            "Sleeping %s seconds after each host check to avoid hitting AT API limits", KITE_AT_API_SLEEP_SECS)
        # The first part is about analyzing ANY host appearing in ANY data source...
        if output:
            with open(os.path.join(APP.config['UPLOAD_FOLDER'], output), 'w', 0) as out_file:
                out_file.write('Hostname,State,Reasons\n')
                for host in self.all_hosts:
                    try:
                        line = api_check_hostname(host.lower())
                        if line:
                            out_file.write(line+'\n')
                    except UnicodeEncodeError as err:
                        logger.warning("Unicode issue somewhere: %s", err)
                    # To avoid hitting AT API throttling limits
                    time.sleep(KITE_AT_API_SLEEP_SECS)
        else:
            # Whereas this second part is directly working on Airtable, so it only cares about what's being reported by Airtable in the first place.
            for item in QAT.aggregate_view['records']:
                if 'Service End Date' in item['fields']:
                    continue  # Skip already decommissioned nodes. No point in analyzing them
                isNotReconciled = 'Reconciliation' not in item['fields']
                isNotOK = item['fields'].get('Reconciliation') != 'OK'
                # Default is to do any non OK, otherwise with --fast is to do not reconciled ones only, or force ALL active systems to be re-evaulated.
                if (not args.fast and not args.all and isNotOK) or (args.fast and isNotReconciled) or args.all:
                    host = item['fields'].get('Host Name').lower()
                    result = api_check_hostname(host)
                    if result:
                        logger.info(
                            'Reconciliation comment for "%s": %s', host, result)
                        if result.get('status') == 'OK':
                            comment = {u'Reconciliation': 'OK'}
                        else:
                            comment = {u'Reconciliation': result.get('reasons')[
                                0]}
                        logger.info('Will update Kiosk with: %s',
                                    comment['Reconciliation'])
                        QAT.update_record(item, comment)


# Instatiation of the main Kite object. Still named after legacy naming BigBrosk, sniff... :(
QBB = Kite(QO, QAT, QDD, QB)


class QueryForm(Form):
    """Form class for the main page /query"""
    hostname = StringField('hostname', validators=[DataRequired()])
    with_qualys = BooleanField('with_qualys', default=False)


@APP.route('/about', methods=['GET'])
# @AUTH.login_required
def about():
    form = QueryForm()
    version_file = os.path.join(APP.root_path, '../../../commit-hash-date.key')
    with open(version_file, 'r') as f:
        version = f.read()
    committers_file = os.path.join(APP.root_path, '../../../committers.txt')
    with open(committers_file, 'r') as f:
        committers = f.read()
    return render_template('about.html', version=version, committers=committers,
                           title='About',
                           form=form)


@APP.route('/hosts/add', methods=['GET'])
# @AUTH.login_required
def hosts_add():
    form = QueryForm()
    return render_template('host_add.html',
                           title='Add Host',
                           form=form)


@APP.route('/')
def hello():
    return redirect(url_for('query'))


@APP.route('/query', methods=['GET', 'POST'])
# @AUTH.login_required
def query():
    """Main page to get started with Kite"""
    form = QueryForm()
    session['eppn'] = os.environ.get('eppn')
    if request.method == 'POST' and form.validate():
        with_qualys = 1 if request.form.get('with_qualys') else 0
        return redirect(url_for('detail_hostname',
                                hostname=request.form.get('hostname').lower()) +
                        "?with_qualys="+str(with_qualys))
    return render_template('query.html',
                           title='Query',
                           form=form)


@APP.route('/autocomplete', methods=['GET'])
# @AUTH.login_required
def autocomplete():
    search = request.args.get('term')
    logger.debug(search)
    return Response(json.dumps(QBB.all_hosts), mimetype='application/json')


@APP.route('/uploads/oracle.csv', methods=['GET'])
def uploads_oracle_csv():
    form = QueryForm()
    uploads = os.path.join(APP.root_path, APP.config.get('UPLOAD_FOLDER'))
    return send_from_directory(directory=uploads, filename='oracle.csv',
                               title='Update Oracle',
                               form=form)


@APP.route('/reconcile', methods=['GET', 'POST'])
# @AUTH.login_required
def reconcile():
    form = QueryForm()
    """Page to upload the CSV from Oracle to run reconciliation (unfinished work)"""
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        uploaded = request.files['file']
        # if user does not select file, browser also
        # submit a empty part without filename
        if uploaded.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if uploaded and uploaded.filename.endswith('csv'):
            filename = 'oracle.csv'
            AKS.upload_file(body=uploaded.read(), file_name=filename)
            flash('Upload successful. Uploaded filename: ' + filename)
            return redirect(url_for('reconcile',
                                    filename=filename))
    return render_template('reconcile.html', title='Reconcile',
                           form=form)


@APP.route('/vulnerabilities/import_csv', methods=['GET', 'POST'])
# @AUTH.login_required
def import_csv():
    form = QueryForm()
    """Page to upload the Qualys vulnerabilities CSV and import in AirTable"""
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        uploaded = request.files['uploaded']
        # if user does not select file, browser also
        # submit a empty part without filename
        if uploaded.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if uploaded and uploaded.filename.endswith('csv'):
            filename = secure_filename('latest_qualys_report.csv')
            uploaded.save(os.path.join(APP.config['UPLOAD_FOLDER'], filename))
            QAT.import_vuln_csv(csv_dir=APP.config['UPLOAD_FOLDER'])
            flash('Upload successful</a>')
            return redirect(url_for('import_csv',
                                    filename=filename))
        else:
            flash('Invalid file or file extension')
            return redirect(request.url)
    return render_template('import_csv.html', title='Import Vulnerabilities Report from Qualys', form=form)


@APP.route('/hosts/<hostname>', methods=['GET'])
# @AUTH.login_required
def detail_hostname(hostname):
    form = QueryForm()
    logger.debug('Qualys flag passed as parameter: %s',
                 request.args.get('with_qualys'))
    if hostname not in QBB.all_hosts:
        return render_json_as_table(jsonify({'ERROR': hostname+' not found'}), hostname, 'color:red', form=form)

    with_qualys = False
    if request.args.get('with_qualys') == '1':
        with_qualys = True

    response = {}
    # Before you wonder, I did try to array/loop this stuff but nope, doesn't work...
    # import ipdb; ipdb.set_trace()
    if with_qualys:
        logger.debug('Qualys being invoked, loading module')
        pqq = Process(target=execute_returning_to_queue, args=(
            PQUEUE, QQ.get_remediation_tickets_list, hostname,))  # pylint: disable=locally-disabled, line-too-long
    pqqsw = Process(target=execute_returning_to_queue,
                    args=(PQUEUE, QQ.get_sw_inventory, hostname,))
    pqj = Process(target=execute_returning_to_queue, args=(
        PQUEUE, QJ.get_active_jiras, hostname,))
    pqs = Process(target=execute_returning_to_queue,
                  args=(PQUEUE, QS.get_tcg_tickets, hostname,))
    pqo = Process(target=execute_returning_to_queue, args=(
        PQUEUE, QO.get_billing_info, hostname,))
    pqat = Process(target=execute_returning_to_queue,
                   args=(PQUEUE, QAT.get_kiosk_info, hostname,))
    pqdd = Process(target=execute_returning_to_queue,
                   args=(PQUEUE, QDD.get_host_tags, hostname,))

    if with_qualys:
        pqq.start()
    pqqsw.start()
    pqj.start()
    pqs.start()
    pqat.start()
    pqo.start()
    pqdd.start()

    if with_qualys:
        pqq.join()
    pqqsw.join()
    pqj.join()
    pqs.join()
    pqo.join()
    pqat.join()
    pqdd.join()

    while not PQUEUE.empty():
        pair = PQUEUE.get()
        response[pair[0]] = pair[1]

    if response.get('get_kiosk_info'):
        kiosk = response.get('get_kiosk_info').get('fields')
        kiosk[response.get('get_kiosk_info').get('edit')] = ''
    else:
        kiosk = 'N/A'

    if response.get('get_host_tags'):
        tags = []
        subdomain = QDD.get_subdomain_by_hostname(hostname)
        real_hostname = QDD.get_correct_case_hostname(hostname)
        tags = response.get('get_host_tags')
    else:
        tags = 'N/A'
        subdomain = 0
        real_hostname = 0

    if response.get('get_sw_inventory'):
        software_list = response.get('get_sw_inventory')
    else:
        software_list = {}

    vulns = response.get('get_remediation_tickets_list')
    consistency_check_result = Kite.check_support_consistency(response)

    def flash_color(argument):
        switcher = {
            "orange": "warning",
            "red": "danger",
            "green": "success",
            "blue": "info"
        }
        return switcher.get(argument, "info")
    coloring = flash_color(consistency_check_result['color'])
    if consistency_check_result['reasons']:
        for reason in consistency_check_result['reasons']:
            flash(reason)
    return render_template('host_details.html', color=coloring, kiosk=kiosk, hostname=hostname, jira=response.get('get_active_jiras'), snow=response.get('get_tcg_tickets'), tags=tags, subdomain=subdomain, real_hostname=real_hostname, vulns=vulns, billing=response.get('get_billing_info'), software=software_list, form=form)


@APP.route('/api/scan/<hostname>', methods=['GET'])
# @AUTH.login_required
def api_scan_hostname(hostname):
    form = QueryForm()
    """API method to scan a host"""
    if hostname not in QBB.all_hosts:
        return render_json_as_table(jsonify({'ERROR': hostname+' not found'}),
                                    hostname,
                                    'color:red', form=form)
    result = QQ.start_new_scan(hostname)
    return render_template('scan_started.html', response_text=result, form=form)


@APP.route('/scans', methods=['GET'])
# @AUTH.login_required
def api_scans():
    form = QueryForm()
    """API method to list all scans"""
    results = QQ.get_scans_as_ul_li_list()
    return render_template('scans.html', scans=results, form=form)


@APP.route('/hosts/all', methods=['GET'])
# @AUTH.login_required
def hosts_all():
    form = QueryForm()
    """Full list with hyperlink to all hosts detected by Kite across our datasources, with hyperlink to it"""
    all_hosts = {'qo': QBB.qo.hosts, 'qdd': QBB.qdd.hosts}
    if hasattr(QBB.qb, 'hosts'):
        all_hosts['qb'] = QBB.qb.hosts
    return render_template('hosts_all.html', hosts=QAT.aggregate_view, hosts_total=len(QAT.aggregate_view), hosts_all=all_hosts, form=form)


@APP.route('/api/check/<hostname>', methods=['GET'])
# @AUTH.login_required
def api_check_hostname(hostname):
    """URI endpoint to check on a host compliance (used by reconcile massively)"""
    hostname = hostname.lower()
    response = {}
    status = None
    pqob = Process(target=execute_returning_to_queue,
                   args=(PQUEUE, QO.get_billing_info, hostname,))
    pqat = Process(target=execute_returning_to_queue, args=(
        PQUEUE, QAT.get_kiosk_info, hostname,))  # pylint: disable=locally-disabled, line-too-long
    pqdd = Process(target=execute_returning_to_queue,
                   args=(PQUEUE, QDD.get_host_tags, hostname,))

    pqat.start()
    pqob.start()
    pqdd.start()

    pqob.join()
    pqat.join()
    pqdd.join()
    # Wait for all results to come in.
    while not PQUEUE.empty():
        pair = PQUEUE.get()
        response[pair[0]] = pair[1]
    # Now craft the response
    result = Kite.check_support_consistency(response)
    return result


@APP.route('/api/datadog', methods=['GET'])
# @AUTH.login_required
def datadog():
    
    """Page to recap how many hosts are being configured in DD, across the board"""
    accounts = {}
    for account in QDD.accounts:
        accounts['<a href="https://'+QDD.subdomains[account] +
                 '.datadoghq.com/infrastructure" target="_blank">'+account+'</a>'] = QDD.accounts[account]
    json_output = jsonify({
        'Datadog total hosts': len(QDD.hosts),
        'Datadog total sub-orgs': len(QDD.accounts.keys()),
        'Datadog sub-orgs': accounts
    })
    if request_wants_json():
        return json_output
    return render_json_as_table(json_output, heading='Datadog orgs info', color='')


def execute_returning_to_queue(queue_name, function, *args):
    """Helper to facilitate the parallel processing in Kite"""
    queue_name.put((function.__name__, function(*args)))
#    logger.debug('process done: %s', function.__name__)


if __name__ == '__main__':
    if args.reconcile:
        if args.output_file:
            QBB.bulk_reconcile(args.output_file)
        else:
            QBB.bulk_reconcile()

    if args.interactive:
        import IPython
        IPython.embed()

    debug_mode = False if os.environ.get('KITE_PROD_MODE') == 'TRUE' else True
    APP.run(debug=debug_mode, host='0.0.0.0', threaded=True)
