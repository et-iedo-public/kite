#!/usr/bin/env python

""" Code to retrieve BigFix stuff systematically """
__author__ = 'TCG, Stanford University <tcg_comm@lists.stanford.edu>'
__license__ = 'Apache License 2.0'


# make Credential.py file in script directory, put in
# username = 'name'
# password = 'password'

import os
import platform
import imp
import argparse
import subprocess
import ConfigParser
import requests
import StringIO
from lxml import objectify, etree
from xml.sax.saxutils import escape #, quoteattr
import logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s-%(levelname)s:: %(message)s')
logger = logging.getLogger(__name__)

def xmlescape(data):
    return escape(data, entities={
        "'": "&apos;",
        "\"": "&quot;"
    })

class QBigFix(object):
    username = None
    password = None
    certificate = None
    url = None
    headers = {'content-type': 'text/xml'}
    template = """<x:Envelope xmlns:x="http://schemas.xmlsoap.org/soap/envelope/" xmlns:rel="http://schemas.bigfix.com/Rel$">
        <x:Header/>
        <x:Body xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                            xmlns:xsd="http://www.w3.org/2001/XMLSchema">
            <GetRelevanceResult xmlns="http://schemas.bigfix.com/Relevance">
          <relevanceExpr>{2}</relevanceExpr>
          <username>{0}</username>
          <password>{1}</password>
               </GetRelevanceResult>
      </x:Body>
    </x:Envelope>
    """

    def __init__(self, config='bigfix.ini', certificate_path=None):
        conf = ConfigParser.RawConfigParser()
        if os.path.isfile(config):
            configFilePath = config
            conf.read(configFilePath)
        else:
            buffer = StringIO.StringIO(config)
            conf.readfp(buffer)
        self.internal_gw_ip = conf.get('Config','internal_gw_ip')
        try:
            output = subprocess.check_output("ping -{} 1 {}".format('n' if platform.system().lower()=="windows" else 'c', self.internal_gw_ip), shell=True)
        except Exception as e:
            logger.warn(e)
            logger.warn('Cannot reach {}, assuming we are not in VPN nor campus, will not load qbigfix module'.format(self.internal_gw_ip))
            return
        self.username = conf.get('Config','username')
        self.password = conf.get('Config','password')
        self.certificate_path = conf.get('Config','certificate_path') if not certificate_path else certificate_path
        self.url = conf.get('Config','url')
        try:
            self.timeout = float(conf.get('Config','connection_timeout'))
        except ConfigParser.NoOptionError:
            self.timeout = 30
            logger.debug('Connection timeout for BigFix requests set to be: {}'.format(self.timeout))
        try:
            self.hosts = self.get_all_hosts()
        except (requests.exceptions.ReadTimeout, requests.exceptions.ConnectTimeout, requests.exceptions.ConnectionError):
            logger.warn('Cannot reach BigFix within timeout, will not load module')
            return
    def get_xml_from_expr(self, expression):
        body = self.template.format(self.username, self.password, xmlescape(expression))
        response = requests.post(self.url,data=body,headers=self.headers, verify=self.certificate_path, timeout=self.timeout)
        return objectify.fromstring(response.content.strip(' \n\t'))
    def get_software_info(self, hostname):
        expr = """
            (values of it) of ((results of ((bes properties whose (id of it = (3093,34,1)));
            (bes properties whose (id of it = (3093,34,1)))) ) whose (name of computer of it as lowercase = "{0}" as lowercase))
            """
        return self.get_xml_from_expr(expr.format(hostname))
    def get_software_info_linux(self, hostname):
        expr = """
            (values of it) of ((results of ((bes properties whose (id of it = (3093,306,2)));
            (bes properties whose (id of it = (3093,306,2)))) ) whose (name of computer of it as lowercase = "{0}" as lowercase))
            """
        return self.get_xml_from_expr(expr.format(hostname))
    def get_system_info(self, hostname):
        # expr = """
        #     (cpu of it; device type of it) of bes computers whose (name of it as lowercase = "{0}" as lowercase)
        #     """
        expr = """(cpu of it; device type of it) of bes computers whose (name of it as lowercase = "{0}" as lowercase)"""
        oxml = self.get_xml_from_expr(expr.format(hostname)+'.stanford.edu')
        listing = oxml.Body.xpath('//*/text()')
        return listing
    def get_all_hosts(self):
        oxml = self.get_xml_from_expr('names of bes computers')
        listing = oxml.Body.xpath('//*/text()')
        return sorted([x.lower().replace('.stanford.edu','') for x in listing])

#print etree.tostring(oxml, pretty_print=True)   

#import IPython; IPython.embed()

# Handle results
#print response.content


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--interactive', action='store_true',
        help="don't return, go into Ipython")
    parser.add_argument('-s', '--software', action='store', dest='software', help='hostname to query for software list')
    parser.add_argument('-S', '--system_info', action='store', dest='system_info', help='hostname to query for System details')
    parser.add_argument('-l', '--list_hosts', action='store_true',
        help="List all available hosts in our BigFix account")
    parser.add_argument('-e', '--expression', action='store', dest='expression', help='expression to query for')
    parser.add_argument('-c', '--config', action='store', dest='config_file',
        help='BigFix credentials .py file full path', required=True)
    parser.add_argument('-C', '--certificate-path', action='store', dest='certificate_path',
        help='BigFix certificate for connecting (defaults to localhost or remote s3 buckiet', required=False)
    args = parser.parse_args()

    q = QBigFix(args.config_file, certificate_path=args.certificate_path)

    if args.expression:
        print q.get_xml_from_expr(args.expression)
    elif args.list_hosts:
        print q.hosts
    elif args.system_info:
        print q.get_system_info(args.system_info)
    elif args.software:
        print q.get_software_info(args.software)
    else:
        pass

    if args.interactive:
        import IPython; IPython.embed()