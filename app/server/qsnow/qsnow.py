#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'TCG, Stanford University <tcg_comm@lists.stanford.edu>'
__license__ = 'Apache License 2.0'
"""
    qsnow
    ~~~~~~~~
    Retrieve info from ServiceNow
"""

import ConfigParser
import argparse
import requests
import ast
import pysnow
import pprint
import collections
import StringIO
import smtplib
from time import sleep
from email.mime.text import MIMEText
import os.path
import logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s-%(levelname)s:: %(message)s')
logger = logging.getLogger(__name__)

class QSnow(object):
    api_user=None
    api_pass=None
    instance_url=None
    instance=None
    headers = {"Accept":"application/json"}
    def __init__(self, config='servicenow.ini'):
        conf = ConfigParser.RawConfigParser()
        if os.path.isfile(config):
            configFilePath = config
            conf.read(configFilePath)
        else:
            buffer = StringIO.StringIO(config)
            conf.readfp(buffer)
        credentials=conf.items('Users')[0]
        self.api_user=credentials[0]
        self.api_pass=credentials[1]
        self.instance_url='https://'+conf.get('Instance','url')+'.service-now.com'
        self.instance=pysnow.Client(instance=self.instance_url,
            user=self.api_user,
            password=self.api_pass,
            raise_on_empty=True)
    def _make_get_params(self, params):
        return '?'+'&'.join('{0}={1}'.format(key, value) for key, value in params.items())
    def get_rest(self,uri='/api/now/table/request?sysparm_limit=1'):
        response = requests.get(self.instance_url+uri, auth=(self.api_user, self.api_pass), headers=self.headers )
        if response.status_code != 200:
            logger.error('Status: {0}, Headers: {1}, Error Response: {2}'.format(response.status_code,response.headers,response.json()))
            exit()
        # Decode the JSON response into a dictionary and use the data
        pprint.pprint(response.json())    
    def get(self,table, search_field, search_value,field=None):
        response = self.instance.query(table=table, query={search_field: search_value}).get_one()
        if field:
            return response[field]
        return response
    def convert(self, data):
        if isinstance(data, basestring):
            return str(data)
        elif isinstance(data, collections.Mapping):
            return dict(map(self.convert, data.iteritems()))
        elif isinstance(data, collections.Iterable):
            return type(data)(map(self.convert, data))
        else:
            return data
    def get_tcg_tickets(self, keyword, closedafter=None):
        params = {'keyword': keyword}
        if closedafter:
            params['closedafter'] = closedafter 
        url = self.instance_url+'/api/stu/su_uit_tcg'+self._make_get_params(params)
        response = requests.get(url, auth=(self.api_user, self.api_pass), headers=self.headers )
        if response.status_code != 200: 
            logger.error('Status: %s | Headers: %s | Error Response: %s', response.status_code, response.headers, response.json())
            return None
        # Decode the JSON response into a dictionary and use the data
        logger.info('Status: %s | Headers: %s',response.status_code,response.headers)
        # import IPython; IPython.embed()
        response = self.convert(response.json())
        if 'result' in response:
            response = response['result'][0]
        if 'message' in response and response.get('message') == 'No results found':
            response = None
        return response
    def get_incidents(self, table='incident'):
        params = {'sysparm_limit': '10'}
        url = self.instance_url+'/api/now/table/'+table+self._make_get_params(params)
        response = requests.get(url, auth=(self.api_user, self.api_pass), headers=self.headers )
        if response.status_code != 200: 
            logger.error('Status: %s | Headers: %s | Error Response: %s', response.status_code, response.headers, response.json())
            return None
        # Decode the JSON response into a dictionary and use the data
        logger.debug('Status: %s | Headers: %s',response.status_code,response.headers)
        return response.json()
#        print('Cookies', response.cookies)
    def get_all(self,table,search_field=None, search_value=None,field=None):
#        import IPython; IPython.embed()
        r = self.instance.query(table=table, query={})
        for record in r.get_multiple(limit=10, order_by=['created_on']):
            print(record['number'])
            sleep(1)
    def alert_user(self,email_address, content=None):
        fromaddr = 'golfieri@stanford.edu'
        toaddrs  = email_address
        msg = "I was bored!"
        server = smtplib.SMTP('smtp.stanford.edu:587')  
        server.starttls()
        # Stanford trusts the whole /14...
        server.sendmail(fromaddr, toaddrs, msg)
        server.quit()
        print "done"

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--interactive', action='store_true',
        help="don't return, go into Ipython")
    parser.add_argument('-c', '--configfile', action='store', dest='config_file',
        help='ServiceNow INI file path containing the credentials to use', required=True)
    parser.add_argument('-t', '--tickets', action='store_true',
        help='Get TCG Tickets via dedicated API call')
    parser.add_argument('-k', '--keyword', action='store', dest='keyword',
        help='Table to target')        
    parser.add_argument('-T', '--table', action='store', dest='table',
        help='Table to target')
    args = parser.parse_args()
    
    qsnow=QSnow(config=args.config_file)

    if args.tickets:
        rs = qsnow.get_tcg_tickets(args.keyword)
        for t in rs.keys():
            print rs[t]
    if args.interactive:
        import IPython; IPython.embed()
