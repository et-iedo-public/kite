#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'TCG, Stanford University <tcg_comm@lists.stanford.edu>'
__license__ = 'Apache License 2.0'
"""
    qsnow
    ~~~~~~~~
    Retrieve info from ServiceNow
"""

import os
from jira import JIRA
import argparse
import ConfigParser
import StringIO
import logging
logging.basicConfig(level=logging.ERROR, format='%(asctime)s-%(levelname)s:: %(message)s')
logger = logging.getLogger(__name__)

class JiraTicket(object):
    def __init__(self, fields):
        self.project='TCG'
        self.summary=None
        self.description=None
        self.issuetype={'name': 'Story'}
        # go figure, this is the way it's stored...
        self.salesforceorg=self.customfield_11030={u'id': u'10331',
            u'self': u'https://stanfordits.atlassian.net/rest/api/2/customFieldOption/10331',
            u'value': u'_TCG (internal)'}
        self.assignee=None
        self.labels=None
        self.sprint=None
        self.duedate=None
        for key, value in kwargs.items():
            if hasattr(self, key):
                setattr(self, key, value)
            else:
                raise Exception('{0} is not a supported Jira ticket attribute')
        

class QJira(object):
    jira = None
    jira_instance_url = None
    def __init__(self, config='jira.ini'):
        conf = ConfigParser.RawConfigParser()
        if os.path.isfile(config):
            configFilePath = config
            conf.read(configFilePath)
        else:
            buffer = StringIO.StringIO(config)
            conf.readfp(buffer)
        self.jira_instance_url = conf.get('Config', 'url')
        self.jira = JIRA(conf.get('Config', 'url'), basic_auth=(conf.get('Config', 'username'), conf.get('Config', 'password')))
        del conf
    def get_jiras(self, query, hostname):
        results = []
        issues = self.jira.search_issues(query.format(hostname))
        for issue in issues:
            results += ['<a href="{0}/browse/{1}" target="_blank">{1}: {2}</a>'.format(self.jira_instance_url,issue.key,issue.fields.summary)]
        return results
    def get_active_jiras(self, hostname):
        return self.get_jiras('text ~ "{0}" AND status != Done AND status != Closed AND summary !~ "Qualys Sev-*" order by created DESC', hostname)
    def get_closed_jiras(self, hostname):
        return self.get_jiras('text ~ "{0}" AND (status = Done OR status = Closed) AND summary !~ "Qualys Sev-*" order by created DESC', hostname)
    def create_jira(self, jira_ticket):
        return self.jira.create_issue(project='PROJ_key_or_id', summary='New issue from jira-python',
                              description='Look into this one', issuetype={'name': 'Bug'})

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--interactive', action='store_true',
        help="don't return, go into Ipython")
#    parser.add_argument('-c', '--configfile', action='store', dest='config_file',
#        help='Datadog INI file path containing all APP and API keys', required=True)
    parser.add_argument('-H', '--host', action='store', dest='host',
        help='host to query for Datadog info', required=True)
    args = parser.parse_args()

    jira=QJira()
    print "ACTIVE jiras:"
    print jira.get_active_jiras(args.host)
    print "DONE jiras:"
    print jira.get_closed_jiras(args.host)
#        print '{0}/browse/{1}'.format(jira.jira_instance_url,issue.key)
#    issue = jira.issue('TCG-1144')
#    print issue.fields.project.key             # 'JRA'
#    print issue.fields.issuetype.name          # 'New Feature'
#    print issue.fields.reporter.displayName    # 'Mike Cannon-Brookes [Atlassian]'
#    print issue.fields
#    print issue.raw
    if args.interactive:
        import IPython
        IPython.embed()