
#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'TCG, Stanford University <tcg_comm@lists.stanford.edu>'  
__license__ = 'Apache License 2.0'  
"""
    pydrac
    ~~~~~~~~
    Retrieve the service console for the given hostname or IP by iterating
    through potential
"""

domains=['.console.sunet','.stanford.edu']
suffixes=['-drac','-idrac','-ipmi','-console']
ports=[':443',':8443',':9443']
uris=['/login.html', '']

import logging
logging.basicConfig(level=logging.DEBUG, format='%(asctime)s-%(levelname)s:: %(message)s')

import os
from flask import Flask, request, session, g, redirect, url_for, abort, render_template, flash, jsonify, make_response
import urllib2
import socket
import httplib
from httplib import HTTPConnection, HTTPS_PORT
import ssl
import sys
import requests
from flask_httpauth import HTTPBasicAuth

class HTTPSConnection(HTTPConnection):
    "This class allows communication via SSL."
    default_port = HTTPS_PORT
    def __init__(self, host, port=None, key_file=None, cert_file=None,
            strict=None, timeout=socket._GLOBAL_DEFAULT_TIMEOUT,
            source_address=None):
        HTTPConnection.__init__(self, host, port, strict, timeout,
                source_address)
        self.key_file = key_file
        self.cert_file = cert_file
    def connect(self):
        "Connect to a host on a given (SSL) port."
        sock = socket.create_connection((self.host, self.port),
                self.timeout, self.source_address)
        if self._tunnel_host:
            self.sock = sock
            self._tunnel()
        # this is the only line we modified from the httplib.py file
        # we added the ssl_version variable
        self.sock = ssl.wrap_socket(sock, self.key_file, self.cert_file, ssl_version=ssl.PROTOCOL_TLSv1)
#now we override the one in httplib
httplib.HTTPSConnection = HTTPSConnection
# ssl_version corrections are done


class Inspector(object):
    def inspectHost(self, hostname):
        requests.packages.urllib3.disable_warnings()
        try:
            _create_unverified_https_context = ssl._create_unverified_context
        except AttributeError:
            # Legacy Python that doesn't verify HTTPS certificates by default
            pass
        else:
            # Handle target environment that doesn't support HTTPS verification
            ssl._create_default_https_context = _create_unverified_https_context
        
        class GetOutOfLoop( Exception ):
            pass
        
        response=None
        host_and_port=None
        host_uri=None
        try:
            for domain in domains:
                for suffix in suffixes:
                    for port in ports:
                        for uri in uris:
                            host_port=str(hostname)+suffix+domain+port
                            logging.debug('{0}{1}'.format(host_port,uri))
                            try:
                                conn = httplib.HTTPSConnection(host_port, timeout=3)
                                conn.request("GET", uri)
                            except (socket.gaierror, socket.timeout, socket.error, httplib.HTTPException):
                                continue
                            response = conn.getresponse()
                            if response.status==200:
                                host_and_port=host_port
                                host_uri=uri
                                raise GetOutOfLoop
                            if response.status==302:
                                continue #TBD better be following 
        except GetOutOfLoop:
            pass 
        if not host_and_port:
            return None
        return 'https://'+host_port+host_uri

# applEdzPFcJ7uMzOs is the prod instance!
def compute_at (filename, api_key, at_id='app3pW3xLi2nRL6VN'):
    import numpy
    import pandas
    from airtable import airtable
    at = airtable.Airtable(at_id, api_key)
    items=at.get('Aggregate View')
    item_id=None
    while True:
#        import pdb; pdb.set_trace()
        for item in items['records']:
            if 'Service Console' not in item['fields']:
                service_console=inspector.inspectHost(item['fields']['Host Name'])
                if service_console:
                    updated_data={u'Service Console': service_console}
                    at.update('Aggregate View', str(item['id']), updated_data)
        if 'offset' in items:
            items=at.get('Aggregate View',offset=str(items['offset']))
        else:
            break

if __name__ == '__main__' and len(sys.argv)==1:
    # create our little application :)
    app = Flask(__name__)
    auth = HTTPBasicAuth()
    inspector=Inspector()
    app.config.from_object(__name__)
    # Load default config and override config from an environment variable
    app.config.from_envvar('FLASKR_SETTINGS', silent=True)

    @auth.get_password
    def get_password(username):
        if username == 'tcg': return 'pleaseaccessme!'
        return None
    @auth.error_handler
    def unauthorized():
        return make_response(jsonify({'error': 'Unauthorized access'}), 401)

    @app.errorhandler(404)
    def not_found(error):
        return make_response(jsonify({'error': 'Not found'}), 404)

    @app.route('/')
    def show_entries():
        return render_template('show_entries.html', entries=results)

    @app.route('/api/drac/<hostname>', methods=['GET'])
    @auth.login_required
    def api_drac_hostname(hostname):
        return jsonify({'response': inspector.inspectHost(hostname)})
    @app.route('/find/<hostname>')
    def find_hostname(hostname):
        result=inspector.inspectHost(hostname)
        if result.startswith('http'):
            return redirect(result, 301)
        else:
            return "<html><h1>Can't find a guessable service console for {0}, sorry!</h1></html>".format(hostname)
    context = ('cert.pem', 'key.pem')
    app.run(debug=True, threaded=True, ssl_context=context)
    
elif len(sys.argv)==3:
    compute_at(sys.argv[1], sys.argv[2])
else:
    pass