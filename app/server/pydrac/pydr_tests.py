import pydr

import flask
import os
import unittest
import tempfile

class PydrTestCase(unittest.TestCase):

    def setUp(self):
        return

    def tearDown(self):
        return
    
    def test_empty_db(self):
        rv = self.app.get('/')
        assert b'No entries here so far' in rv.data
        
if __name__ == '__main__':
    unittest.main()
    
    
app = flask.Flask(__name__)

with app.test_request_context('/?name=mydas'):
    assert flask.request.path == '/'
    assert flask.request.args['hostname'] == 'mydas'
