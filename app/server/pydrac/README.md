First: create your key/cert:
> openssl req -x509 -newkey rsa:2048 -keyout key.pem -out cert.pem -days 3650
...
Country Name (2 letter code) [AU]:US
State or Province Name (full name) [Some-State]:California
Locality Name (eg, city) []:Stanford
Organization Name (eg, company) [Internet Widgits Pty Ltd]:Stanford University
Organizational Unit Name (eg, section) []:TCG
Common Name (e.g. server FQDN or YOUR name) []:golfieri-mbpro.stanford.edu      
Email Address []:golfieri@stanford.edu

Then remove the PITA:
> openssl rsa -in key.pem -out key.pem 

To run the dynamic webapp discoverer based on Flask:
> python pydr.py

Then:

To connect with a browser, go to https://<hostname>:5000

To run a comparative analysis between a local csv file and the real thing online:
> python pydr.py kiosk_dump.csv `cat ~/work/secrets/airtable.key`
> python pydr.py kiosk_dump2.csv `cat ~/work/secrets/airtable.key`

To inquiry via API for a specific host:
> curl -k https://localhost:5000/api/drac/fors-amcom-rc1 -u'tcg:pleaseaccessme!'

