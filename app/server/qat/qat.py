#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'TCG, Stanford University <tcg_comm@lists.stanford.edu>'
__license__ = 'Apache License 2.0'
"""
    qat
    ~~~~~~~~
    Retrieve info from Airtable
"""
import sys
import os
from flask import Flask, request, session, g, redirect, url_for, abort, render_template, flash, jsonify, make_response
from flask_httpauth import HTTPBasicAuth
from airtable import airtable
import ConfigParser
import json
import StringIO
import numpy
import pandas
import re
from subprocess import Popen, PIPE
import argparse
import logging
logging.info('Starting logger')
logging.getLogger("urllib3").setLevel(logging.WARNING)
logger = logging.getLogger(__name__)

HOSTS_TABLE='Systems'

class AirTableApi(object):
    aggregate_view = None
    hosts = []
    __api_key = None
    at = None
    base_id = None
    table_id = None
    view_id = None
    vuln_base_id = None
    schema = None
    HOSTNAME_LABEL = None
    HOSTS_TABLE = None
    def __init__(self, config='airtable.ini', schema_json='airtable_schema.json'):
        self._set_parameters(config=config, schema_json=schema_json)
        self._set_airtable_api_object()
        self._populate()
    def _set_parameters(self, config, schema_json, api_key=None, base_id=None,  table_id=None, view_id=None, vuln_base_id=None):
        conf = ConfigParser.RawConfigParser()
        if os.path.isfile(config):
            configFilePath = config
            conf.read(configFilePath)
        else:
            buffer = StringIO.StringIO(config)
            conf.readfp(buffer)
        # Loads schema for airtable since APIs suck enough not to support recursivity or
        # support calls to fetch schema...
        if os.path.isfile(str(schema_json)):
            with open(schema_json,'r') as f:
                self.schema = json.loads(f.read())
        else:
            self.schema = schema_json
        logger.info('Schema to structure Airtable data: {0}'.format(self.schema))

        if api_key:
            self.__api_key = api_key.rstrip()
        else:
            self.__api_key = conf.get('Authentication', 'base_key')
        # Since the primary field could be renamed and break havoc, it's been parameterized
        self.HOSTS_TABLE = conf.get('Configuration', 'hosts_table')
        self.HOSTNAME_LABEL = conf.get('Configuration', 'primary_key_field')
        self.base_id = base_id if base_id else conf.get('Authentication', 'base_id')
        self.table_id = table_id if table_id else conf.get('Authentication', 'table_id')
        self.view_id = view_id if view_id else conf.get('Authentication', 'view_id')
        self.vuln_base_id = vuln_base_id if vuln_base_id else conf.get('Authentication', 'vuln_base_id')
        del conf
    def _set_airtable_api_object(self):
        self.at = airtable.Airtable(self.base_id, self.__api_key)
    def _populate(self, hostname=None):
        if hostname in self.hosts and self.aggregate_view:
            record = filter(lambda r: r['fields'].get(self.HOSTNAME_LABEL) == hostname, self.aggregate_view['records'])[0]
            updated_record = self.at.get(self.HOSTS_TABLE, str(record['id']))
            for r in self.aggregate_view['records']: # could have made a unique for loop around self.aggregate_view, but too late... too lazy
                if r['id'] == record['id']:
                    r['fields'] = updated_record['fields']
            return
        results = self.aggregate_view=self.at.get(self.HOSTS_TABLE)
        if 'records' not in self.aggregate_view:
            logger.error('Cannot retrieve records from AirTable, please try again')
            exit(0)
        while 'offset' in results:
            tranche=self.at.get(self.HOSTS_TABLE,offset=str(self.aggregate_view['offset']))
            results['records']+=tranche['records']
            if 'offset' in tranche:
                results['offset']=tranche['offset']
            else:
                del results['offset']
        # import IPython; IPython.embed()
        self.aggregate_view['records'] = sorted(results['records'], key=lambda x: x['fields']['Host Name'])
#        logger.info('Field name used for primary host name: {0}'.format(self.HOSTNAME_LABEL))
#        logger.info('FIRST record fetched (for troubleshooting purposes): {0}'.format(self.aggregate_view['records'][0]))
#        logger.info('Hostname out of First record: {0}'.format(self.aggregate_view['records'][0]['fields'].get(self.HOSTNAME_LABEL)))
        # note: this next line will fail if there is a record without a 'Host Name' (e.g. blank line)
        self.hosts=[h['fields'].get(self.HOSTNAME_LABEL).replace('.stanford.edu','')
            for h in self.aggregate_view['records'] if h['fields'].get(self.HOSTNAME_LABEL) not in (None, '')
        ]
        self.hosts.sort()

#   Helper method to get a list of unique values and their count
    def kiosk_groupby_field(self, getfield):
        groups = [h['fields'].get(getfield)
            for h in self.aggregate_view['records'] if h['fields'].get(getfield) is not None
        ]
        summary={}
        for i in groups:
            if not summary.has_key(i): summary[i]=1  #also: if not i in d

            else: summary[i]+=1
        return summary

    def update_record(self, item, updated_data):
        return_code = self.at.update(self.HOSTS_TABLE, str(item['id']), updated_data)
        logger.info('Airtable updated, return code: %s', return_code)
        return return_code
    def get_kiosk_info(self, hostname):
        self._populate(hostname) # just in case something changed
        hostname = hostname.lower()
        logger.debug('Retrieving %s from AT', hostname)
        for record in self.aggregate_view['records']:
            _hostname= record['fields'].get(self.HOSTNAME_LABEL)
            if _hostname is not None and _hostname==hostname:
#                logger.debug('record BEFORE: {0}'.format(record))
                for field, value in record['fields'].items():
                    linked=[]
                    # This means the item is somehow a darn subrecord
                    if field in self.schema[self.HOSTS_TABLE]:
                        if self.schema[self.HOSTS_TABLE][field]!=None:
                            # This means that we care about fetching it
    #                        logger.debug('field {0} has been found in schema, proceeding with extra fetch'.format(field))
                            for item in value:
    #                            logger.debug('value of AT item: {0}'.format(value))
                                if value[0].startswith('rec'):
                                    # It means it refers to a child table, fetching
                                    linked_fields=self.at.get(self.schema[self.HOSTS_TABLE][field],str(value[0]))
                                    if 'fields' in linked_fields:
    #                                    logger.debug('RAW linked_fields: {0}'.format(linked_fields))
                                        f=linked_fields['fields']
                                        linked=[str(f[x]) for x in f if not type(f[x])==list]
    #                                    logger.debug('linked_fields: {0}'.format(linked))
                                        record['fields'][field]=', '.join(linked)
                                else:
                                    record['fields'][field]=str(value[0])
                        else:
                            # it means it's set to None
#                            logger.debug('deleting: {0}'.format(field))
                            del record['fields'][field]
                    if field==self.HOSTNAME_LABEL:
                        logger.debug('processing {0}: {1}'.format(field,value.encode('utf-8')))
#                logger.debug('record AFTER: {0}'.format(record))
                record['fields']['at_table_id'] = self.table_id
                record['fields']['at_view_id'] = self.view_id
                record['fields']['at_id'] = record.get('id')
                return record
        logger.debug('Could not find %s among %s records retrieved from AT', hostname, len(self.aggregate_view['records']))
        return None
    def import_vuln_csv(self, csv_dir=None, csv_name='latest_qualys_report.csv', table='Kite Import'):
        transient_csv='out.csv'
        uploaded=csv_dir+'/'+csv_name
        with open(uploaded, "r") as in_file, open(transient_csv, "w") as out_file:
            sub = Popen(['sed','-E', '-n',
                '-e', '/^\r?$/d',
                '-e', '/^"IP","DNS"/,$p',
                uploaded], stdout=out_file)
            sub.wait()
        with open(transient_csv, "r") as transient:
            df=pandas.read_csv(transient)
        df=df[df['Severity']>3]
        df.dropna(thresh=10, inplace=True)
        df=df.replace(numpy.nan, '', regex=True)
        # We need to have all strings as it plays better with Airtable APIs
        df.Port = df.Port.astype(str)
        df.Severity = df.Severity.astype(str)
        df.QID = df.QID.astype(str)
        df['Times Detected']=df['Times Detected'].astype(str)

        at = airtable.Airtable(self.vuln_base_id, self.__api_key)
        for index, row in df.iterrows():
            logger.debug('creating: %s',row)
            updated_data=dict(zip(df.columns, row.values))
#            logger.debug('data: %s',updated_data)
            result=at.create(table, updated_data)
#            logger.debug('result: %s',result)
#            import IPython; IPython.embed()
    def get_support_info(self, hostname):
        # I need to fix this method, currently we have Service Level as name, and it is a linked record
        result=self.get_kiosk_info(hostname)
        if result is None or \
            'fields' not in result or \
            'Support Type' not in result['fields'] or \
            result['fields']['Support Type'] is None:
            return 'None'
        else:
            return result['fields']['Support Type']

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--interactive', action='store_true',
        help="don't return, go into Ipython")
    parser.add_argument('-k', '--keyfile', action='store', dest='key_file',
        help='Airtable Key file path', required=True)
    parser.add_argument('-H', '--host', action='store', dest='host',
        help='host to query for Kiosk info')
    parser.add_argument('-l', '--list', action='store_true',
        help='list hosts in Kiosk')
    args = parser.parse_args()

    at=AirTableApi(args.key_file)
    if args.host:
        result = at.get_kiosk_info(args.host)
    if args.list:
        result = at.aggregate_view
    print result

    if args.interactive:
        import IPython; IPython.embed()
