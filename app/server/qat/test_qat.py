#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'TCG, Stanford University <tcg_comm@lists.stanford.edu>'  
__license__ = 'Apache License 2.0'  
"""
    qat - Testing module
    ~~~~~~~~
    Retrieve info from Airtable
"""

import sys
import pytest
import json
import qat
import logging
logging.basicConfig(level=logging.DEBUG, stream=sys.stdout)
logger = logging.getLogger(__name__) # pylint: disable=locally-disabled, invalid-name

from mock import patch
try:
    from mock import MagicMock, Mock
    import mock
except ImportError:
    from unittest.mock import MagicMock, Mock
    from unittest import mock

# https://stackoverflow.com/questions/29318987/preferred-way-of-patching-multiple-methods-in-python-unit-test
@pytest.mark.usefixtures("aggregate_view", "config_file") # I don't think I need config_file anymore, but anyhoo
@pytest.fixture(scope="function") # has to be function-scoped since mocker is, and can't be redefined/overriden
def qat_object(aggregate_view, secret_file, mocker):
    _set_airtable_api_object_mock = mocker.patch.object(qat.AirTableApi, '_set_airtable_api_object', autospec=True)
    _set_parameters_mock = mocker.patch.object(qat.AirTableApi, '_set_parameters', autospec=True)
    _populate_mock = mocker.patch.object(qat.AirTableApi, '_populate', autospec=True)
    o = qat.AirTableApi()
    o.HOSTNAME_LABEL='Host Name'
    o.HOSTS_TABLE='Systems'
    o.schema = json.loads('''
{"Systems":
    {"Service Level": "Hours & Rates",
     "Which TCG Engineer Designed (or on-boarded)? (Linked)": "Contacts",
     "Owner Schools or Departments": "Schools & Departments",
     "Client Org": "Client Orgs",
     "Business Contact SUNet (Linked)": "Contacts",
     "Technical Contact SUNet (Linked)": "Contacts",
     "Patching Assignment": "Contacts",
     "Business FN (Linked)": "",
     "Business LN": "",
     "Business email (Linked)": "",
     "Tech FN (Linked)": "",
     "Tech LN (Linked)": "",
     "Tech email (Linked)": ""
    }
}
    ''')
    o.aggregate_view = aggregate_view
    return o


@pytest.mark.usefixtures("qat_object")
class TestQAT(object):
    def test_fetch_right_amount_of_records(self, qat_object):
        assert len(qat_object.aggregate_view['records']) == 2
    def test_get_kiosk_info(self, qat_object):
        assert qat_object.get_kiosk_info('host2')['fields']['OS'] == 'Linux'
    def test_get_kiosk_info_2(self, qat_object):
        assert qat_object.get_kiosk_info('host1')['fields']['Service Level'] == 'slid3'