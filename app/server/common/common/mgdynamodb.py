#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'TCG, Stanford University <tcg_comm@lists.stanford.edu>'  
__license__ = 'Apache License 2.0'
"""
    mgdynamodb
    ~~~~~~~~
    common tool to relate to dynamodb (could have used a community provided one, but wanted to learn)
"""

import os
import sys
import boto3
import argparse
import logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

class MGDynamoDB(object):
    db = None
    cache = None
    default_attr = None
    def __init__(self, table, key, default_attr, profile_name=None, key_id=None, region_name='us-west-2',
                 aws_access_key_id=None, aws_secret_access_key=None):
        self.db = boto3.resource('dynamodb')
        self.cache = self.db.Table(table)
        self.key = key
        self.default_attr = default_attr
        # if aws_access_key_id or aws_secret_access_key:
        #     self.kms = boto3.client('kms', region_name=region_name,
        #                             aws_access_key_id=aws_access_key_id, aws_secret_access_key=aws_secret_access_key)
        #     self.s3=boto3.client('s3', region_name=region_name,
        #                          aws_access_key_id=aws_access_key_id, aws_secret_access_key=aws_secret_access_key)
        # else:
        #     self.kms = boto3.client('kms', region_name=region_name)
        #     self.s3=boto3.client('s3', region_name=region_name)
        # self._data_key = None
        #        logger.info('Access key being used: %s',session.get_scoped_config()['aws_access_key_id'])
    def get(self, key, target_attr = None):
        Key = { self.key: key }
        try:
            response = self.cache.get_item(Key=Key)
            payload = response.get('Item')
            if target_attr:
                logger.info('Found cached entry of type %s for %s: %s', target_attr, key, payload.get(target_attr))
                return payload.get(target_attr)
            else:
                logger.info('Found cached entry of default type %s for %s: %s', self.default_attr, key, payload.get(self.default_attr))
                return payload.get(self.default_attr)
        except Exception as e:
            # As there's a factory for botocore.errorfactory.ResourceInUseException, didn't know how else to handle it
            if 'ResourceNotFoundException' == e.__class__.__name__:
                return None
    def set(self, key, attrs):
        # Will update record without warning if already exists! For a fast lookup cache it's perfect :-)
        Item = { self.key: key }
        Item.update(attrs)
        Item = {x:str(y) for (x,y) in Item.items()} # making sure everything is string before using it
        logger.info('Caching {}'.format(Item))
        response = self.cache.put_item(Item=Item)
        return response

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--interactive', action='store_true',
        help="don't return, go into Ipython")
    parser.add_argument('-D', '--demo', action='store_true',
        help="run extra test code and showcase get/put/count stuff")
    args = parser.parse_args()

    if args.demo:
        cache = MGDynamoDB('kite_hosts', 'hostname', 'asset_id')
        print(cache.set('mgtestmg', {'asset_id': '12345678','first':'First', 'last':'Last'}))
        print(cache.get('mgtestmg'))
        print(cache.set('mgtestmg', {'first':'First', 'last':'Last'}))
        print(cache.set('mgciaomg', {'first':'First', 'last':'LastLast'}))

    if args.interactive:
        import IPython; IPython.embed()