#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'TCG, Stanford University <tcg_comm@lists.stanford.edu>'  
__license__ = 'Apache License 2.0'  
"""
    Configuration
    ~~~~~~~~
    Common code to handle all configuration aspects of each datasource, incl. secrets
"""

class Configuration(object):
    'Common base class for all datasources configuration'
    config_file=None

    
        
    