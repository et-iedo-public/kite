#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'TCG, Stanford University <tcg_comm@lists.stanford.edu>'  
__license__ = 'Apache License 2.0'
"""
    kms_s3_encrypt
    ~~~~~~~~
    tool to encode our secrets using KMS and storing them to S3
"""

import os
import sys
import shutil
import gzip
import logging
import botocore.session
import boto3
import base64
import argparse
import json
import StringIO
from Crypto.Cipher import AES
from Crypto import Random
# for fix to python on mac which is newer than the one on linux

logging.basicConfig(stream=sys.stdout, level=logging.INFO)
logging.getLogger("urllib3").setLevel(logging.WARNING)
logger = logging.getLogger(__name__) # pylint: disable=locally-disabled, invalid-name

# Helpers for AES encryption
BS = 16
pad = lambda s: s + (BS - len(s) % BS) * chr(BS - len(s) % BS) 
unpad = lambda s : s[:-ord(s[len(s)-1:])]

def sep(msg, length=80):
    print '#'*length+'\n# {0}\n'.format(msg)+'#'*length

class AwsSecret(object):
    def __init__(self, profile_name, key_id, bucket_name, region_name='us-west-2',
                 aws_access_key_id=None, aws_secret_access_key=None):
        self.key_id = key_id
        if not key_id:
            raise Exception('need a data key, run generate_data_key or pass one as arg')
        # Set your profile name on a low-level Botocore session
        session = botocore.session.get_session()
        boto3.session.profile = profile_name
        # Tell Boto 3 to use that session by default
        boto3.setup_default_session(botocore_session=session)
        if aws_access_key_id or aws_secret_access_key:
            self.kms = boto3.client('kms', region_name=region_name,
                                    aws_access_key_id=aws_access_key_id, aws_secret_access_key=aws_secret_access_key)
            self.s3=boto3.client('s3', region_name=region_name,
                                 aws_access_key_id=aws_access_key_id, aws_secret_access_key=aws_secret_access_key)
        else:
            self.kms = boto3.client('kms', region_name=region_name)
            self.s3=boto3.client('s3', region_name=region_name)
        self.bucket=bucket_name
        self._data_key = None
#        logger.info('Access key being used: %s',session.get_scoped_config()['aws_access_key_id'])

    def generate_data_key(self, key_spec='AES_256'):
        '''Creates data key in KMS account so that we can do envelope encryption.
        Later on, only the user allowed via AWS policies will be able to retrieve and
        decrypt such key'''
        response = self.kms.generate_data_key(KeyId=self.key_id, KeySpec=key_spec)
        if response['ResponseMetadata']['HTTPStatusCode'] == 200:
            self._data_key = response
#            logger.info('self._data_key generated: %s', response)
            return response['Plaintext']
        # if you cannot generate the symmetric key itself, something is wrong with your
        # credentials...bail out
        raise Exception('Error while generating data key: {0}'.format(response))

    def get_plaintext_symmetric_key(self, data_key_enc=None):
        '''Retrieve the plaintext symmetric data key we just created for envelope enc'''
        if self._data_key is None and data_key_enc is None:
            logger.info('generating data key as self._data_key is empty')
            self_data_key = self.generate_data_key()
        enc_key= data_key_enc if data_key_enc else self._data_key['CiphertextBlob']
        logger.debug('enc_key: %s', enc_key)
        response = self.kms.decrypt(CiphertextBlob=enc_key)
        if response['ResponseMetadata']['HTTPStatusCode'] == 200:
            return response['Plaintext']
        else:
            raise Exception('no pre-existing data key')

    def env_encrypt(self, message, data_key=None):
        '''Encrypt message with data key (this is done client-side)'''
        # envelope encryption, should be default for most cases!
        plaintext_enc_key = data_key if data_key else self.get_plaintext_symmetric_key()
        if type(message) == file:
            message = message.read()
#        logger.info('message from file: %s', message)
        raw = pad(message)
        iv = Random.new().read( AES.block_size )
        cipher = AES.new( plaintext_enc_key, AES.MODE_CBC, iv )
        return base64.b64encode( iv + cipher.encrypt( raw ) )
    
    def env_decrypt(self, message, data_key):
        '''Decrypt the message client-side'''
        plaintext_enc_key = base64.b64decode(self.get_plaintext_symmetric_key(data_key))
        logger.debug('plaintext_enc_key: %s', plaintext_enc_key)
        enc = base64.b64decode(message)
        iv = enc[:16]
        cipher = AES.new(plaintext_enc_key, AES.MODE_CBC, iv )
        return unpad(cipher.decrypt( enc[16:] ))    
        
    def cmk_encrypt(self, message):
        '''Server-side encryption relying on master keys in KMS.  We cannot use this
        for secrets bigger than 4K, but it's actually a pretty generous limit as
        the great majority of secrets can fit in that. Still, best practice would be
        to rely solely on envelope encryption and do it client-side.'''
        # good for small secrets, but even then, it's better to rely on env enc
        if type(message) == file:
            message = message.read()
        return self.kms.encrypt(KeyId=self.key_id, Plaintext=message)

    def cmk_decrypt(self, message, data_key_enc=None):
        '''Server-side decryption. See cmk_encrypt method doc string for details'''
        return self.kms.decrypt(CiphertextBlob=message)#['Plaintext']
    
    def list_files(self):
        '''list all objects in bucket'''
        response = self.s3.list_objects_v2(
            Bucket=self.bucket,
            Delimiter='=',
        )
        return response

    def upload_encrypted_file_with_encrypted_data_key(self, payload, file_name, data_key_enc=None):
        '''uploads freshly encrypted file via envelope encryption and
        sets metadata to store it with it'''
        enc_key = data_key_enc if data_key_enc else self._data_key['CiphertextBlob']
        return self.s3.put_object(Bucket=self.bucket, Body=payload, Key=file_name,
            Metadata={'encryption-key': base64.b64encode(enc_key)})

    def upload_encrypted_payload(self, payload, file_name):
        '''uploads freshly encrypted file via CMK encryption'''
        return self.upload_file(body=payload['CiphertextBlob'], file_name=file_name)

    def upload_file(self, body, file_name):
        '''uploads freshly encrypted file via CMK encryption'''
        return self.s3.put_object(Bucket=self.bucket, Body=body, Key=file_name)

    def download_file(self,file_name, file_out=None):
        '''downloads encrypted file, simple as that'''
        logger.info('reading remote file %s', file_name)
        payload = self.s3.get_object(Bucket=self.bucket, Key=file_name)
        if file_out:
            with open(file_out,'w') as f:
                f.write(payload.get('Body').read())
        else:
            return payload

    def gunzip_file(self, gz_file, dest_file, remove_original_gz_file=False):
        '''Gunzips file'''
        if os.path.isfile(dest_file):
            return None
        with gzip.open(gz_file, 'rb') as f_in:
            with open(dest_file, 'wb') as f_out:
                shutil.copyfileobj(f_in, f_out)
        if remove_original_gz_file and os.path.isfile(dest_file):
            os.remove(gz_file)

    def s3_secrets_write(self, payload, secret_store_name):
        '''Wrapper to upload a s3 secret (either file or string) into a file on S3 encrypted'''
        encrypted=self.cmk_encrypt(payload)
        self.upload_encrypted_payload(encrypted, secret_store_name)
        logger.info('secret_store_file_name: %s', secret_store_name)

    def s3_secrets_read(self, file_name):
        '''Wrapper to download and retrieve a secret from S3 encrypted'''
        download_response = self.download_file(file_name)
        encrypted_data_body = download_response.get('Body').read()
#        logger.debug('encrypted_data_body: %s', encrypted_data_body)
        return self.cmk_decrypt(encrypted_data_body).get('Plaintext')

    def s3_secrets_dump_to_file(self, file_name, file_out):
        '''Helper to dump contents from encrypted S3 object into file on FS'''
        contents=self.s3_secrets_read(file_name)
        with open(file_out,'w') as f:
            f.write(contents)

    def s3_secrets_read_json_as_dict(self, file_name):
        '''Wrapper to download and retrieve a secret from S3 encrypted, structured as json at the source, and returning a dict'''
        raw_json = self.s3_secrets_read(file_name)
        return json.load(StringIO.StringIO(raw_json))
        
if __name__ == '__main__':
    ENCRYPTED_FILE='envelope_encrypted_data.txt'
    PLAINTEXT='Hi! I am a text craving to be encrypted!\nFind me in TCG!\nMarcello'
    KMS_IAM_USER='bigbrosk'
    S3_SECRETS_BUCKET='tcg-bigbrosk-secrets'
    MASTER_KEY='f529c4bf-2b51-48d0-9cba-a43ab7181ab4'
    
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--interactive', action='store_true',
        help="don't return, go into Ipython")
    parser.add_argument('-n','--name', dest='file_name',
        help='Object name of the encrypted information')
    parser.add_argument('-D','--demo', action='store_true',
        help='Showcase of all actions that could be done with this libraray')
    parser.add_argument('-u','--upload', dest='file_upload_full_path',
        help='Full path to file to upload')
    parser.add_argument('-d','--download', action='store_true',
        help='Fetch secret object contents')
    parser.add_argument('-l','--list', action='store_true',
        help='List all files in bucket')
    parser.add_argument('-o','--output-file', dest='output_file',
        help='Outputs to file')
    args = parser.parse_args()
    
    ak=AwsSecret(KMS_IAM_USER,MASTER_KEY, S3_SECRETS_BUCKET)

    if args.list:
        print 'listing files:'
        for i in ak.list_files()['Contents']:
            print i['Key']

    if args.file_upload_full_path:
        with open(args.file_upload_full_path,'rb') as body:
            sep('uploading file {0}'.format(args.file_upload_full_path))
            ak.s3_secrets_write(body, args.file_name)
            sys.exit(0)

    if args.download:
        if args.output_file:
            ak.s3_secrets_dump_to_file(args.file_name, args.output_file)
        else:
            print ak.s3_secrets_read(args.file_name)
        sys.exit(0)

    if args.demo:
        sep('Direct encryption with Customer Master key aka CMK (requires message to be <4KB)')
        blob=ak.cmk_encrypt(PLAINTEXT)
            # If I were to use CLI:
            # aws kms encrypt --key-id alias/kite-masterkey  --plaintext file:///etc/asl.conf --query CiphertextBlob --output text | base64 --decode > blob.b64
        print 'encrypted blob=>  {0}'.format(blob)

        sep('Direct decryption with CMK')
        dec=ak.cmk_decrypt(blob['CiphertextBlob'])
            # If I were to use CLI (note the fileb:// for binary format):
            # aws kms decrypt --ciphertext-blob fileb://blob.b64 --query Plaintext --output text | base64 --decode 
        print 'decrypted blob=>  {0}'.format(dec)

        sep('Envelope encryption (AES256)')
        print 'plaintext data key=>  {0}'.format(ak.get_plaintext_symmetric_key())
        blob=ak.env_encrypt(PLAINTEXT)
        print 'encrypted blob=>  {0}'.format(blob)

        sep('Upload to S3 with encrypted key as metadata')
        upload_status=ak.upload_encrypted_file_with_encrypted_data_key(blob,ENCRYPTED_FILE,None)
        print(upload_status)

        sep('Download encrypted envelope data from S3 with its associated data key found in metadata')
        download_response=ak.download_file(ENCRYPTED_FILE)
        print(download_response)
        data_key=download_response.get('Metadata').get('encryption-key')
        print 'data key=>  {0}'.format(data_key)
        encrypted_data_body=download_response.get('Body').read()
        print 'body=>  {0}'.format(encrypted_data_body)

        sep('Envelope decryption')
        dec=ak.env_decrypt(encrypted_data_body, data_key)
        # If I were to use CLI (note the fileb:// for binary format):
        # aws kms decrypt --ciphertext-blob fileb://blob.b64 --query Plaintext --output text | base64 --decode 
        print 'decrypted blob=>  {0}'.format(dec)

        sep('S3 playground')
        print 'listing files:'
        for i in ak.list_files()['Contents']:
            print i['Key']

    if args.interactive:
        import IPython
        IPython.embed()