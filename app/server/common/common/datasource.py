#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'TCG, Stanford University <tcg_comm@lists.stanford.edu>'  
__license__ = 'Apache License 2.0'  
"""
    Datasource
    ~~~~~~~~
    Common code to all datasource classes in the rest of this project
"""

class Datasource(object):
    'Common base class for all datasources'
    hosts=None
    datasources_count=0
    def __init__():
        Configuration.datasources_count+=1
    def __del__():
        Configuration.datasources_count-=1
    def get_datasources_count(self):
        return Datasource.datasources_count
    def list_hosts():
        pass
    
    