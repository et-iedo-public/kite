#!/usr/bin/env python

""" Code to retrieve Airtable stuff systematically """
__author__ = 'TCG, Stanford University <tcg_comm@lists.stanford.edu>'
__license__ = 'Apache License 2.0'


import os
import sys
from optparse import OptionParser
from flask import Flask
import logging
logging.basicConfig(stream=sys.stdout, level=logging.DEBUG, format='%(asctime)s %(message)s')
logger = logging.getLogger(__name__)
sys.path.append('../server/qat')
import qat

def main():
    """main routine"""

    # argv
    parser = OptionParser()
    parser.add_option("-a", "--airtable",
                      dest="airtable", default=False, action="store_true",
                      help="Query Airtable (Kiosk) for hosts. Requires a valid ../../config/airtable.key")
    (options, dummy) = parser.parse_args()

    # argument is required
    if not (options.airtable):
        parser.print_help()

    # Print hosts in Airtable/Kiosk
    if options.airtable:
        kiosk = qat.AirTableApi(config_file='../../config/airtable.key')
        print kiosk.get_kiosk_info('swlic')
#        print kiosk.hosts
#        for host in sorted(kiosk.hosts, key=lambda s: s.lower()):
#            print host

if __name__ == '__main__':
    main()

