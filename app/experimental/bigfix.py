#!/usr/bin/env python

""" Code to retrieve BigFix stuff systematically """
__author__ = 'TCG, Stanford University <tcg_comm@lists.stanford.edu>'
__license__ = 'Apache License 2.0'


# make Credential.py file in script directory, put in
# username = 'name'
# password = 'password'

import imp
import argparse
import requests
from lxml import objectify, etree
from xml.sax.saxutils import escape #, quoteattr

def xmlescape(data):
    return escape(data, entities={
        "'": "&apos;",
        "\"": "&quot;"
    })


relevanceExpr = 'names of bes computers'
# Installed applications - Windows 
relevanceExpr2 = """
exists regapp "qualysagent.exe"
**OR**
exists keys whose (exists value "DisplayName" whose (it as string as lowercase contains "qualys cloud security agent") of it) of keys "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall" of (x64 registries;x32 registries)
"""
relevanceExpr3= """names of bes computers"""
relevanceExpr4= """
(exists keys whose (exists value "DisplayName" whose (it as string as lowercase contains "qualys cloud security agent") of it) of keys "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall" of (native registries; x32 registries)) or (exists file "/usr/local/qualys/cloud-agent/bin/qualys-cloud-agent")
"""
relevanceExpr5 = """(values of it) of ((results of ((bes properties whose (id of it = (3093,34,1))); (bes properties whose (id of it = (3093,34,1)))) ) whose (name of computer of it as lowercase = "argus" ))"""
relevanceExpr6 = """(values of it) of ((results of (bes properties whose (id of it = (3093,34,1)))) whose (name of computer of it as lowercase = "aperturedb2" ))"""


class QBigFix(object):
    username = None
    password = None
    certificate = None
    url = None
    headers = {'content-type': 'text/xml'}
    template = """<x:Envelope xmlns:x="http://schemas.xmlsoap.org/soap/envelope/" xmlns:rel="http://schemas.bigfix.com/Rel$">
        <x:Header/>
        <x:Body xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                            xmlns:xsd="http://www.w3.org/2001/XMLSchema">
            <GetRelevanceResult xmlns="http://schemas.bigfix.com/Relevance">
          <relevanceExpr>{2}</relevanceExpr>
          <username>{0}</username>
          <password>{1}</password>
               </GetRelevanceResult>
      </x:Body>
    </x:Envelope>
    """
    def __init__(self, config_file=None, username=None, password=None, certificate=None, url=None):
        if config_file:
            bf_creds = imp.load_source('bigfix_creds', args.config_file)
            self.username = bf_creds.username
            self.password = bf_creds.password
            self.certificate = bf_creds.certificate
            self.url = bf_creds.url
        else:
            self.username = username
            self.password = password
            self.certificate = certificate
            self.url = url
    def get_xml_from_expr(self, expression):
        body = self.template.format(self.username, self.password, xmlescape(expression))
        response = requests.post(self.url,data=body,headers=self.headers, verify=self.certificate)
        return objectify.fromstring(response.content.strip(' \n\t'))
    def get_host_info(self, hostname):
        expr = """
            (values of it) of ((results of ((bes properties whose (id of it = (3093,34,1)));
            (bes properties whose (id of it = (3093,34,1)))) ) whose (name of computer of it as lowercase = "{0}" ))
            """
        oxml = self.get_xml_from_expr(expr.format(hostname))
    def get_all_hosts(self):
        oxml = self.get_xml_from_expr('names of bes computers')
        b=[]
        for i in oxml.iter():
            b.append(i)
        return b

#print etree.tostring(oxml, pretty_print=True)   

#import IPython; IPython.embed()

# Handle results
#print response.content


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--interactive', action='store_true',
        help="don't return, go into Ipython")
    parser.add_argument('-H', '--hostname', action='store', dest='hostname', help='hostname to query for')
    parser.add_argument('-l', '--list_hosts', action='store_true',
        help="List all available hosts in our BigFix account")
    parser.add_argument('-e', '--expression', action='store', dest='expression', help='expression to query for')
    parser.add_argument('-c', '--config', action='store', dest='config_file',
        help='BigFix credentials .py file full path', required=True)
    args = parser.parse_args()

    q = QBigFix(args.config_file)

    if args.expression:
        print q.get_xml_from_expr(args.expression)
    elif args.list_hosts:
        print q.get_all_hosts()
    elif args.hostname:
        print q.get_host_info(args.hostname)
    else:
        pass

    if args.interactive:
        import IPython; IPython.embed()