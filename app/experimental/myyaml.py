#!/usr/local/bin/python
# -*- coding: utf-8 -*-

""" Code to learn pyyaml """
__author__ = 'TCG, Stanford University <tcg_comm@lists.stanford.edu>'
__license__ = 'Apache License 2.0'

import os
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
import yaml
import StringIO
import logging
import argparse
sys.path.append('../server/common/common')
import kms_s3_encrypt

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--interactive', action='store_true',
        help="don't return, go into Ipython")
    parser.add_argument('-f', '--yamlfile', action='store', dest='yamlfile',
        help='optional output file for bulk reconciliation mode. If unspecified, defaults to remote file consistency_matrix.yaml on the S3 bucket')
    args = parser.parse_args()

    yamlfile=None
    if os.path.isfile(str(args.yamlfile)):
        with open(args.yamlfile, 'r') as f:
            yamlfile = f.read()
    else:
        AKS=kms_s3_encrypt.AwsSecret(os.environ['KITE_KMS_IAM_USER'],
            os.environ['KITE_MASTER_KEY'],
            os.environ['KITE_S3_SECRETS_BUCKET'],
            aws_access_key_id=os.environ.get('aws_access_key_id'),
            aws_secret_access_key=os.environ.get('aws_access_key_id'))
        yamlfile=AKS.s3_secrets_read('consistency_matrix.yaml')

    matrix = yaml.safe_load(yamlfile)

    print matrix

    responses = []
    responses.append({'get_kiosk_info': {'fields': {'Service End Date': 'x/y/z', 'Service Level': 'Critical (247, 1hr Response)', 'Bill Code': 'TCG PLEX BIZ' }},
        'get_host_tags': ['support:biz'],
        'get_billing_info': 'TCG PLEX BIZ'}) # Decomm'ed
    responses.append({'get_kiosk_info': {'fields': {'Service Level': 'Critical (247, 1hr Response)', 'Bill Code': 'TCG PLEX BIZ' }},
        'get_host_tags': ['support:biz'],
        'get_billing_info': 'TCG STRD 247'}) # Kiosk/oracle mismatch in billing
    responses.append({'get_kiosk_info': {'fields': {'Service Level': 'Critical (247, 1hr Response)', 'Bill Code': 'TCG PLEX BIZ' }},
        'get_host_tags': ['support:biz'],
        'get_billing_info': 'TCG STRD BIZ'}) # Right support type, wrong billing
    responses.append({'get_kiosk_info': {'fields': {'Service Level': 'Critical (247, 1hr Response)', 'Bill Code': 'Comm Svcs Flat Rate' }},
        'get_host_tags': ['support:247'],
        'get_billing_info': False}) # Flat rate
    responses.append({'get_kiosk_info': {'fields': {'Service Level': 'Critical (247, 1hr Response)', 'Bill Code': 'TCG FLEX' }},
        'get_host_tags': ['support:none'],
        'get_billing_info': 'TCG FLEX'}) # Flex, no support
    responses.append({'get_kiosk_info': {'fields': {'Service Level': 'Critical (247, 1hr Response)', 'Bill Code': 'TCG STRD 247' }},
        'get_host_tags': ['support:247'],
        'get_billing_info': 'TCG STRD 247'}) # OK 247
    responses.append({'get_kiosk_info': {'fields': {'Service Level': 'Critical (247, 1hr Response)', 'Bill Code': 'TCG STRD BIZ' }},
        'get_host_tags': ['support:biz'],
        'get_billing_info': 'TCG STRD BIZ'}) # OK BIZ


    print responses

#   Service end date NOT Empty -> DONE (no need to check further)
#   Oracle not showing payment info
#       -> IF Flat rate in Kiosk (e.g. VAST)
#           -> Check Datadog shows support:247 (currently no Flat rate implies biz support AFAIK?)
#       -> IF NOT Flat rate in Kiosk:
#           -> better be support:none (e.g. Farallone)
#   Oracle shows payment info
#       -> Validate it is same as Kiosk Bill Code
#       -> Is datadog tag in line with payment?
#           -> If FLEX: better be support:none datadog tag
#           -> If a BIZ rate:
#               -> Payment info should sport BIZ suffixed at the end
#               -> [Just alerting, not triggering an error] Check service criticality in line with support requested
#           -> If a 247 rate:
#               -> Payment info should sport BIZ suffixed at the end
#               -> [Just alerting, not triggering an error] Check service criticality in line with support requested
#               -> [Just alerting, not triggering an error] Check that hostname does not have the following keywords: test, dev, uat, stag

    def s(response, result):
            print('[[ {0} ]] => \n\t{1}\n'.format(response, result))

    for response in responses:
        print('*** EVALUATING {}'.format(response))
        if response.get('get_kiosk_info'): 
            kiosk=response.get('get_kiosk_info').get('fields')
            service_level = kiosk.get('Service Level')
        else: # can't get AirTable data. Given it's so central, won't continue further
            s(response, {'status': 'ERROR', 'color': 'red', 'reasons': 'AirTable information not found'})
        oracle_billing = response.get('get_billing_info') if response.get('get_billing_info') else 'None'
        kiosk_billing = kiosk.get('Bill Code') if kiosk.get('Bill Code') else False
        datadog_support_tag = filter(lambda x: x.startswith('support:'), response.get('get_host_tags'))

        result={'status': 'ERROR', 'color': 'red', 
            'reasons' : ['Invalid configuration (Oracle: {}, Kiosk Bill code: {}, DD: {}, Service: {}'.format(
                oracle_billing, kiosk_billing, datadog_support_tag, service_level)]}

        if kiosk.get('Service End Date'): # Host has been decommissioned.
            result.update({'status': 'DECOMM','color': 'gray', 'reasons' : ['Decommissioned']})
        # elif oracle_billing != kiosk_billing:
        #     result.update({'reasons' : ['Billing mismatch between Kiosk and Oracle']})
        else:
            def matching(real,expected):
                if isinstance(real,list) and len(real) > 0:
                    real = real[0]
                expected = [] if (expected is False or expected is None) else expected
                return True if (real in expected) else False
            for category in sorted(matrix): # Sorted so I can evaluate in the order I need by prepending the right index number
                combo = matrix[category]
                logger.info('Kiosk Billing match? {} (between {} and {}'.format(matching(kiosk_billing, combo['kiosk_billing']), kiosk_billing, combo['kiosk_billing']))
                logger.info('Oracle Billing match? {} (between {} and {}'.format(matching(oracle_billing, combo['oracle_billing']), oracle_billing, combo['oracle_billing']))
                logger.info('DD match? {} (between {} and {}'.format(matching(datadog_support_tag, combo['datadog']), datadog_support_tag, combo['datadog']))
                # import IPython; IPython.embed()
                if (matching(kiosk_billing, combo['kiosk_billing']) and 
                    matching(oracle_billing, combo['oracle_billing']) and 
                    matching(datadog_support_tag, combo['datadog'])):
                        result.update({'status': 'OK', 'color': 'green', 'reasons' : []})
                        break
        s(response,result)

    if args.interactive:
        import IPython; IPython.embed()