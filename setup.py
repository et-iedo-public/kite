#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'TCG, Stanford University <tcg_comm@lists.stanford.edu>'  
__license__ = 'Apache License 2.0'  
"""
    Kite setup.py
    ~~~~~~~~
    Sets up kite app requirements.  See app/server/kite/README.md too.
    See https://stanfordits.atlassian.net/browse/TCG-424 for details.
"""

from setuptools import setup, find_packages
from pip.req import parse_requirements
from pip.download import PipSession
import os

__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))

VERSION = "1.0.0"

def read_requirements():
    '''parses requirements from requirements.txt'''
    reqs_path = os.path.join(__location__, 'requirements.txt')
    install_reqs = parse_requirements(reqs_path, session=PipSession())
    reqs = [str(ir.req) for ir in install_reqs]
    return reqs

setup(
    name = "kite",
    version = VERSION,
    setup_requires = ['numpy'],
    install_requires = read_requirements(),
    packages = find_packages(exclude=["test*"]),
    author = "Marcello Golfieri",
    author_email = "golfieri@stanford.edu",
    description = ("A python Flask app collecting info regarding TCG-managed"
                 "assets at Stanford from multiple sources"
                 "Note, you need gfortran installed on Mac:"
                 "http://coudert.name/software/gfortran-6.1-ElCapitan.dmg"),
    license = "apache2",
    url = "https://code.stanford.edu/tcg/kite"
)