#!/usr/bin/env bash
WORKDIR=`dirname $0`
PROJECT=`basename $(pwd)`
cd ${WORKDIR}/..
echo Connecting to `cat .last_container_id_started`
source config/${PROJECT}-dev.sh
if [[ $# -eq 1 ]] ; then 
	echo Executing command ${1} on ${PROJECT}_${PROJECT}:latest	
	exec docker exec \
		-e BB_JIRA_USERNAME -e BB_JIRA_PASSWORD -e AWS_ACCESS_KEY_ID -e AWS_SECRET_ACCESS_KEY \
		-it ${1} /bin/bash
else
	echo Connecting to latest running container of ${PROJECT}:
	exec docker exec \
		-e BB_JIRA_USERNAME -e BB_JIRA_PASSWORD -e AWS_ACCESS_KEY_ID -e AWS_SECRET_ACCESS_KEY \
	       	-it `cat -s .last_container_id_started`  /bin/bash
fi