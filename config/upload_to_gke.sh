#!/usr/bin/env bash
cd `dirname $0`
source ../*env/bin/activate
source kite-prod.sh
cd ..
#source venv/bin/activate
pytest -v -s || { echo Tests failed}; exit 1; }
git log --max-count=1 > commit-hash-date.key
git shortlog -s -n --all | head -10 > committers.txt
docker build . -t gcr.io/tcg-files/kite:latest || { echo Building image failed}; exit 2; }
gcloud docker -- push gcr.io/tcg-files/kite:latest || { echo Uploading image failed}; exit 3; }
kubectl set image deployments/kite-deployment kite=gcr.io/tcg-files/kite:latest -n kite-prod || { echo refreshing image deployment failed}; exit 4; }
# This is a trick to force a redeployment. Maybe there's a smarter way? I coulnd't find it... well except for tagging every
# as it would be best practice... :-)
sed "s/DEPLOYMENT_TRIGGERED_TS_.*/DEPLOYMENT_TRIGGERED_TS_$(date +%y%m%d_%H%m%S)/" config/k8s/deployment.yaml | kubectl apply -n kite-prod -f -
watch kubectl describe deployments/kite-deployment -n kite-prod
