#!/usr/bin/env bash
WORK=`dirname $0`
PROJECT=kite
NAMESPACE=kite-prod
cd $WORK
kubectl delete services --all --namespace=${NAMESPACE}
kubectl delete ingress --all --namespace=${NAMESPACE}
kubectl delete deployments --all --namespace=${NAMESPACE}
