#!/usr/bin/env bash
WORK=`dirname $0`
PROJECT=kite
NAMESPACE=kite-prod
cd $WORK
# Initially created secrets in default, so had to copy them to new namespace:
# kubectl get rs,secrets -o json --namespace default | jq '.items[].metadata.namespace = "kite-prod"' | kubectl create -f  -
kubectl create -f ns-${NAMESPACE}.yaml
ubectl -n ${NAMESPACE} create serviceaccount helm-client
kubectl config set-context ${NAMESPACE} \
	--namespace=${NAMESPACE} \
	--cluster gke_tcg-files_us-west1-a_kite-cluster \
	--user gke_tcg-files_us-west1-a_kite-cluster
kubectl config use-context ${NAMESPACE}
kubectl create -f deployment.yaml --namespace=${NAMESPACE}
kubectl create -f service.yaml --namespace=${NAMESPACE}
kubectl create -f ingress.yaml --namespace=${NAMESPACE}

