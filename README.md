# WARNING/DISCLAIMER

This is abandonware, I uploaded this to avoid being forgetting this behind completely. 
When I moved on to other roles at Stanford, I had no more need for this data aggregator anymore.
All submodules of Kite still are being used elsewhere though.
Marcello Golfieri

# Install

To install Kite, just check out the repo:

> git clone git@code.stanford.edu:tcg/kite.git

Now, you need to manually create the uploads/ and config/ dirs, and get the requisite secrets:
- airtable.key contains just the key for AirTable
- config.ini contains the username and key for Qualys
- datadog.ini contains all the api and app keys for all our DD accounts
- servicenow.ini contains the username and api key for the dev Service Now
- setup.ini contains a list of authorized Users to access the system (user=pwd format)

Currently, we are shifting to a KMS/IAM/S3 backed up secrets store in a specific S3 bucket.  To
upload secrets, you need to have rights to write to such bucket, and then:

> python ~/work/kite/app/server/common/common/kms_s3_encrypt.py -u ~/work/kite/config/CONFIG_FILE_NAME -n CONFIG_FILE_NAME
> [[ $? -eq 0 ]] && rm ~/work/kite/config/CONFIG_FILE_NAME

For example:

> python ../common/common/kms_s3_encrypt.py -u ~/work/kite/config/setup.ini -n setup.ini
> [[ $? -eq 0 ]] && rm ~/work/kite/config/setup.ini

In uploads/, you will need oracle.csv.

On top of that, you need to create two environment variables in your current shell for Jira:
KITE_JIRA_USERNAME
KITE_JIRA_PASSWORD
We should use the TCG Robot user credentials for this one.

You also need to install pip:

> sudo easy_install pip

And make sure to open a new shell to enjoy reflecting the new PATH to pip

If you do not already have it installed, you should both install and use virtualenvs from here onward (adjust paths as you see fit):

> sudo pip install virtualenv

> mkdir ~/virtualenv

> cd ~/virtualenv

> virtualenv venv

> source ~/virtualenv/venv/bin/activate

Within your virtualenv, then install all your requirements:

> pip install enum34

> pip install -r requirements.txt

You may find alternate versions of requirements files specific to your OS. If available, try using those first.

# Run it

Then, to run it, just either do the docker or native approaches:

## Native instructions

Then it's as easy as:

> cd app/server/kite ; python kite.py

## Docker instructions (local development)

First of all you need to populate the config with the needed passwords and uploads dir ON YOUR COMPUTER.
Then you can build the docker image (if not already available somewhere else by the time I'm done here):

> docker build -t kite .

And eventually you can now start the docker with the required volume mappings.
The following script deals with them:

> sh run.sh

To cleanup stopped containers and outdated images:

> docker ps -aq --no-trunc | xargs docker rm
> docker images -q --filter dangling=true | xargs docker rmi


## [OUTDATED as Kubernetes was used!!!!] Docker instructions (Rancher steps)

First you have to have a bash file with the following to be sourced beforehand:

```bash
export AWS_ACCESS_KEY_ID=
export AWS_SECRET_ACCESS_KEY=
export KITE_KMS_IAM_USER='bigbrosk'
export KITE_S3_SECRETS_BUCKET='tcg-bigbrosk-secrets'
export KITE_MASTER_KEY=
export KITE_UPLOADS_DIR=/uploads
export KITE_CONFIG_FOLDER=/config
export RANCHER_URL=
export RANCHER_ACCESS_KEY=
export RANCHER_SECRET_KEY=
```

Then run:

> /Applications/rancher-compose up --upgrade --force-recreate -c

If something goes wrong, just roll-back:

> /Applications/rancher up -r

If it complains that the status got stuck in a previous run, you need to delete the stack.
Also remember to update haproxy to allow traffic to this new container, and route53 to resolve
the name kite.tcg.stanford.edu to the public ranche IP address

# Usage

The "main" page is here:

http://localhost:5000/query

The login credentials can be found in config/setup.ini.

Now, to query Kite fetching for a host full info:
> curl http://localhost:5000/api/info/humbot

If you don't need qualys, you better skip it by setting with_qualys to 0, e.g.:
> curl http://localhost:5000/api/info/humbot?with_qualys=0

To check for basic consistency among support, oracle and datadog tags:
> curl http://localhost:5000/api/check/humbot

To execute the bulk reconcile feature, you need to go CLI:
> python kite.py reconciliation.csv

# Alexa feature

To use Alexa, you need to have the application available publicly on 443 https.  But for
development, you could use [ngrok](https://ngrok.com) (e.g. `brew cask install ngrok`), and then:

> /Applications/ngrok http 5000

When you get the https url, just set it in the alexa developer console on AWS.

To query, just say "Alexa, start Kite". When asked for a host, say one of the many to get the support level.
