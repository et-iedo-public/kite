#!/usr/bin/env bash
pids="$(ps aux | grep -v grep | awk '/kite.py/ { print $2}')"
pids="$pids $(ps aux | grep -v grep | awk '/\/kite\// { print $2}')"

for p in $pids; do
    kill -9 $p
done
