FROM python:2.7-slim
# TODO: replace /app/server with an ENV variable
# All of this to get env variables from docker-compose.yaml into Dockerfile
# See https://github.com/docker/compose/issues/1837#issuecomment-316896858
ENV REQUIREMENTS_TXT requirements.txt
ARG APACHE_CONFIG_FILE=kite.conf
ENV APACHE_CONFIG_FILE "${APACHE_CONFIG_FILE}"
ARG KITE_PROD_MODE
ENV KITE_PROD_MODE "${KITE_PROD_MODE}"

RUN apt-get update --fix-missing && apt-get install -y \
    curl vim less iputils-ping jq unzip \
    shibboleth-sp2-common shibboleth-sp2-utils \
    libapache2-mod-shib2 apache2 libapache2-mod-wsgi \
    libxml2-dev libxslt1-dev libz-dev \
    python-pip python-dev build-essential \
    libssl-dev libffi-dev libcurl4-openssl-dev
ADD ./${REQUIREMENTS_TXT} ./
RUN pip install --process-dependency-links -r /${REQUIREMENTS_TXT}
RUN a2enmod shib2
RUN a2enmod rewrite
RUN a2enmod ssl
RUN rm /etc/apache2/sites-available/000-default.conf
RUN rm /etc/apache2/sites-enabled/000-default.conf
RUN curl -k https://uit.stanford.edu/sites/default/files/2017/02/07/attribute-map.xml > /etc/shibboleth/attribute-map.xml
ADD ./config/docker/${APACHE_CONFIG_FILE} /etc/apache2/sites-available/kite.conf
ADD ./config/docker/sp-config-kite.xml /etc/shibboleth/shibboleth2.xml
RUN ln -s /etc/apache2/sites-available/kite.conf /etc/apache2/sites-enabled/kite.conf
RUN mkdir -p /opt/tcg/secrets
RUN mkdir -p /var/www/tcg
WORKDIR /var/www/tcg
# The packages are in order of likelihood to be touched
# so that we minimize caches rebuilds
ADD ./config/docker/kite.wsgi ./kite.wsgi
ADD ./healthcheck.sh ./healthcheck.sh
ADD ./run.sh ./run.sh
RUN chmod a+x ./healthcheck.sh
RUN chmod a+x ./run.sh
RUN mkdir ./uploads
RUN chmod 0777 ./uploads
ADD ./app/server/qsnow ./app/server/qsnow
ADD ./app/server/pydrac ./app/server/pydrac
ADD ./app/server/qoracle ./app/server/qoracle
ADD ./app/server/qjira ./app/server/qjira
ADD ./app/server/qbigfix ./app/server/qbigfix
ADD ./app/server/common/common ./app/server/common/common
ADD ./app/server/qat ./app/server/qat
ADD ./app/server/qqualys ./app/server/qqualys
ADD ./app/server/qdatadog ./app/server/qdatadog
ADD ./app/server/kite ./app/server/kite
RUN chmod +rx ./app/server/kite/static/favicon.ico
RUN unzip -d ./app/server/kite/static/ ./app/server/kite/static/kite_icons.zip
RUN rm ./app/server/kite/static/kite_icons.zip
RUN chmod +rx ./app/server/kite/static/*.png
ADD ./commit-hash-date.key ./commit-hash-date.key
ADD ./committers.txt ./committers.txt
EXPOSE 8000
CMD ["./run.sh", "$KITE_PROD_MODE"]
