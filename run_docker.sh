#!/usr/bin/env bash
WORK=$(cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd)
echo $WORK
if [ "${KITE_PROD_MODE}x" == "TRUEx" ]; then
	environment=prod
	# no source, as the env vars are inherited via docker-compose.yml when ranchering up
else
	environment=dev
	source config/kite-${environment}.sh
fi

docker run -d -it -p 80:80 -p 443:443 \
	-e AWS_ACCESS_KEY_ID \
	-e AWS_SECRET_ACCESS_KEY \
	kite_kite &&
	echo `docker ps --last 1  -q |cut -f1 -d' '` > .last_container_id_started
docker logs -f `cat .last_container_id_started`
