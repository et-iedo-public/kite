#!/usr/bin/env bash
WORK=`dirname $0`
PROJECT=kite
cd $WORK
if [ "${KITE_PROD_MODE}x" == "TRUEx" ]; then
	environment=prod
	# no source, as the env vars are inherited via docker-compose.yml when ranchering up
else
	environment=dev
	source config/${PROJECT}-${environment}.sh
fi
SECRETS_DIR=/opt/tcg/secrets
echo Run environment: ${environment}
if [ -f /.dockerenv ]; then
    echo "Running in container, fetching secrets:";
    python app/server/common/common/kms_s3_encrypt.py -d -n ${PROJECT}-sp-cert.pem -o ${SECRETS_DIR}/sp-cert.pem
    python app/server/common/common/kms_s3_encrypt.py -d -n ${PROJECT}-sp-key.pem -o ${SECRETS_DIR}/sp-key.pem
    chmod 400 ${SECRETS_DIR}/sp-key.pem
    service shibd restart
    python app/server/common/common/kms_s3_encrypt.py -d -n ${PROJECT}-env-vars.py -o ${SECRETS_DIR}/env-vars.py
    service apache2 restart
    tail -f /var/log/apache2/${PROJECT}_error.log
else
    echo "Non-container run"
    cd app/server/${PROJECT}
    KITE_UPLOADS_DIR=../../../uploads python ${PROJECT}.py
fi
