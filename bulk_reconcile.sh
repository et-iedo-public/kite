#!/usr/bin/env bash
WORK=`dirname $0`
cd $WORK
environment=dev
source config/kite-${environment}.sh
cd app/server/kite
KITE_UPLOADS_DIR=../../../uploads python kite.py -R $@ 
