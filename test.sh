#!/usr/bin/env bash
WORK=`dirname $0`
cd $WORK
source kite_env/bin/activate
export PYTHONPATH=$PITHONPATH:../qoracle:../qsnow:../pydrac:../qjira:../qdatadog:../qqualys:../qat
pylint --rcfile=${ROOT}/pylintrc kite.py
pytest -v -s
