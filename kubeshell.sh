#!/usr/bin/env bash
# prioritizes a non-starting pod if available. That would show as 0/1 ready in kubectl get output
if [ "$1" = "-d" ]; then
  DEBUG='-r'
fi 
POD=$(kubectl get pods -n kite-prod | grep kite-deployment | sort -n ${DEBUG} -k 2 | tail -n 1)
echo Connecting to ${POD}
TARGET=$(echo ${POD} | awk '{print $1}')
kubectl exec -it ${TARGET} -n kite-prod -- /bin/bash
